<?php
if(!isset($_GET["rid"]))
		{
				
			 header("location:dynamicadmin.php");
		}
		else {
				$curdate=date("Y/m/d");
				include ('db.php');
				$id = $_GET['rid'];
				
				
				$sql ="Select * from poolreservation where id = '$id'";
				$re = mysqli_query($con,$sql);
				while($row=mysqli_fetch_array($re))
				{
					$uname = $row['uname'];
					$fname = $row['fname'];
					$lname = $row['lname'];
					$email = $row['email'];
					$resortname = $row['resortname'];
					$hourstay = $row['hourstay'];
					$cnum = $row['cnum'];
					$addon = $row['add on'];
					$cin = $row['cin'];
					$stat = $row['stat'];

				}
			}
		?> 

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
  	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  	<meta name="description" content="">
  	<meta name="author" content="">

    <title>Admin Pool Book Dashboard</title>
    <link rel="icon"  href="../image/icon/admin.png">


    <!-- Bootstrap core CSS -->
  	<link href="css/bootstrap/bootstrap.min.css" rel="stylesheet">
  	<!-- MDB BOOTSTRAP -->
  	<link rel="stylesheet" type="text/css" href="css/mdb/mdb.min.css">
  	<!-- Custom styles for this template -->
  	<link href="css/admincss.css" rel="stylesheet">
  	<!-- FONT AWESOME -->
  	<link rel="stylesheet" type="text/css" href="fonts/font-awesome.css">
    <!-- Google Fonts-->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />

</head>

<body>
<!--Navbar -->
<nav class="navbar navbar-expand-lg blue-gradient">
  <a class="navbar-brand black-text" href="#">PRIVADO <?php echo $_COOKIE['resort']; ?></a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarDropdownMenuLink-333"
    aria-controls="navbarDropdownMenuLink-333" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarDropdownMenuLink-333">
    <ul class="navbar-nav ml-auto nav-flex-icons">
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle white-text" id="navbarDropdownMenuLink-333" data-toggle="dropdown" aria-haspopup="true"
          aria-expanded="false">Manage
        </a>
        <div class="dropdown-menu dropdown-default" aria-labelledby="navbarDropdownMenuLink-333">
          <a class="dropdown-item" href="logout.php">Log out</a>
        </div>
      </li>
    </ul>
  </div>
</nav>
<!-- Navbar -->
<div class="d-flex" id="wrapper">

    <!-- Sidebar -->
    <div class="elegant-color border-right" id="sidebar-wrapper">
      <div class="list-group list-group-flush">
        <a href="dynamicadmin.php" class="list-group-item list-group-item-action "><i class="fa fa-dashboard"></i> Dashboard</a>
        <a href="poolbook.php" class="list-group-item list-group-item-action blue-gradient white-text"><i class="fa fa-bookmark"></i> Pool Booking</a>
        <a href="payment.php" class="list-group-item list-group-item-action "><i class="fa fa-money"></i> Payment</a>
      </div>
    </div>
    <!-- /#sidebar-wrapper -->
    <div class="container heavy-rain-gradient">
      <div class="row">
        <div class="col-12">
          <div class="card text-center mt-2">
            <div class="card-header success-color">
              Pool Reservation <?php echo  $curdate; ?>
            </div>
            <div class="card-body">
              <h5 class="card-title">Reservation Confirmation<span class="badge badge-secondary"></span></h5>
              <table class="table table-striped table-bordered">
                <thead>
                  <tr>
                    <th>DESCRIPTION</th>
                    <th>INFORMATION</th>
                  </tr>
                </thead>
                <tbody>
                	<tr>
                    	<th class="text-warning">Name</th>
                        <th><?php echo $fname."".$lname; ?> </th>    
                    </tr>
					<tr>
                        <th class="text-warning">Email</th>
                        <th><?php echo $email; ?> </th>   
                    </tr>
					<tr>
                        <th class="text-warning">Contact Number </th>
                        <th><?php echo $cnum; ?></th>  
                   	</tr>
					<tr>
                        <th class="text-warning">Resort name </th>
                        <th><?php echo $resortname;  ?></th>   
                    </tr>
					<tr>
                        <th class="text-warning">Hour stay </th>
                        <th><?php echo $hourstay; ?></th>  
                    </tr>
					<tr>
                        <th class="text-warning">Add on </th>
                        <th><?php echo $addon; ?></th>    
                    </tr>
					<tr>
                        <th class="text-warning">Check-in Date </th>
                        <th><?php echo $cin; ?></th>    
                    </tr>
					<tr>
                        <th class="text-warning">Status Level</th>
                        <th><?php echo $stat; ?></th>    
                    </tr>
              	</tbody>
               </table>
               <form method="post">
               	<div class="row">	
	               <div class=" col-6">
					 	<div class="form-group">
							<label>Select the Confirmation</label>
								<select name="status"class="form-control">
									<option value selected></option>
									<option value="Confirmed">Confirm</option>
									<option value="Cancelled">Not confirm</option>
								</select>
						</div>
               		</div>
               		<div class="md-form col-6">
			            <label  class="grey-text font-weight-light">Deposit Payment</label>
			            <input type="text" id="pay" name="pay" class="form-control"  autocomplete="off">
		        	</div>
               		<input type="submit" name="confirm" value="Submit" class="btn aqua-gradient">
               	</div>
               </form>
            </div>
            <div class="card-footer text-muted success-color black-text">
              PRIVADO
            </div>
          </div>
        </div>
      </div>
    </div>
    

	<!-- jQuery-2.2.4 js -->
    <script src="js/jquery/jquery-2.2.4.min.js"></script>
    <!-- Popper js -->
    <script src="js/bootstrap/popper.min.js"></script>
    <!-- Bootstrap-4 js -->
    <script src="js/bootstrap/bootstrap.min.js"></script>

    <!-- SCRIPTS -->
    <!-- JQuery -->
    <script type="text/javascript" src="js/mdb/jquery-3.3.1.min.js"></script>
    <!-- Bootstrap tooltips -->
    <script type="text/javascript" src="js/mdb/popper.min.js"></script>
    <!-- MDB core JavaScript -->
    <script type="text/javascript" src="js/mdb/mdb.js"></script>
    <!-- Bootstrap core JavaScript -->
    <script src="js/adminjs/jquery.min.js"></script>




</body>

</html>

<?php
						if(isset($_POST['confirm']))
						{	
							$st = $_POST['status'];
							$dpay = $_POST['pay'];

							if($st=="Confirmed")
							{
									$urb = "UPDATE `poolreservation` SET `stat`='$st' WHERE id = '$id'";
										
								if( mysqli_query($con,$urb))
								{	
									$rate_of_reservation = 0;
										if($resortname=="Alegria resort")
										{
											if ($hourstay=="12 hrs") {
												$rate_of_reservation = 8500;
											}
											else if ($hourstay=="24 hrs") {
												$rate_of_reservation = 12500;
											}
										}
										else if($resortname=="Carlos resort")
										{
											if ($hourstay =="12 hrs") {
												$rate_of_reservation = 2500;
											}
											else if ($hourstay =="24 hrs") {
												$rate_of_reservation = 5000;
											}
										}
										else if($resortname=="ELW resort")
										{
											if ($hourstay =="12 hrs") {
												$rate_of_reservation = 3000;
											}
										else if ($hourstay =="24 hrs") {
												$rate_of_reservation = 7000;
											}
										}
										else if($resortname=="Floredeliza resort")
										{
											if ($hourstay =="12 hrs") {
												$rate_of_reservation = 8000;
											}
											else if ($hourstay =="24 hrs") {
												$rate_of_reservation = 15000;
											}
										}
										else if($resortname=="INLG resort")
										{
											if ($hourstay =="12 hrs") {
												$rate_of_reservation = 3500;
											}
											else if ($hourstay =="24 hrs") {
												$rate_of_reservation = 9000;
											}
										}
										else if($resortname=="Kayrense resort")
										{
												if ($hourstay =="12 hrs") {
													$rate_of_reservation = 6000;
												}
												else if ($hourstay =="24 hrs") {
													$rate_of_reservation = 9000;
												}
										}
										else if($resortname=="La Azu Thea resort")
										{
												if ($hourstay =="12 hrs") {
													$rate_of_reservation = 8000;
												}
												else if ($hourstay =="24 hrs") {
													$rate_of_reservation = 15000;
												}
										}
										else if($resortname=="Laguna blue resort")
										{
												if ($hourstay =="12 hrs") {
													$rate_of_reservation = 4000;
												}
												else if ($hourstay =="24 hrs") {
													$rate_of_reservation = 8000;
												}
										}
										else if($resortname=="Lenzen Azy resort")
										{
												if ($hourstay =="12 hrs") {
													$rate_of_reservation = 5000;
												}
												else if ($hourstay =="24 hrs") {
													$rate_of_reservation = 7000;
												}
										}
										else if($resortname=="M and A resort")
										{
												if ($hourstay =="12 hrs") {
													$rate_of_reservation = 10000;
												}
												else if ($hourstay =="24 hrs") {
													$rate_of_reservation = 10000;
												}
										}
										else if($resortname=="Mary Grace resort")
										{
												if ($hourstay =="12 hrs") {
													$rate_of_reservation = 7000;
												}
												else if ($hourstay =="24 hrs") {
													$rate_of_reservation = 10000;
												}
										}
										else if($resortname=="Rubi Rosa resort")
										{
												if ($hourstay =="12 hrs") {
													$rate_of_reservation = 2500;
												}
												else if ($hourstay =="24 hrs") {
													$rate_of_reservation = 5000;
												}
										}
										else if($resortname=="Santiago Medina resort")
										{
												if ($hourstay =="12 hrs") {
													$rate_of_reservation = 7000;
												}
												else if ($hourstay =="24 hrs") {
													$rate_of_reservation = 12000;
												}
										}
										else if($resortname=="Valerie resort")
										{
												if ($hourstay =="12 hrs") {
													$rate_of_reservation = 8000;
												}
												else if ($hourstay =="24 hrs") {
													$rate_of_reservation = 15000;
												}
										}
										else if($resortname=="Vergara resort")
										{
												if ($hourstay =="12 hrs") {
													$rate_of_reservation = 3000;
												}
												else if ($hourstay =="24 hrs") {
													$rate_of_reservation = 8000;
												}
										}
										else if($resortname=="Villa Carmencita resort")
										{
												if ($hourstay =="12 hrs") {
													$rate_of_reservation =4000;
										}
												else if ($hourstay =="24 hrs") {
													$rate_of_reservation = 7000;
												}
										}
										else if($resortname=="Villa Jocris 1 resort")
										{
												if ($hourstay =="12 hrs") {
													$rate_of_reservation = 5000;
												}
												else if ($hourstay =="24 hrs") {
													$rate_of_reservation = 8000;
												}
										}
										else if($resortname=="Villa Jocris 2 resort")
										{
												if ($hourstay =="12 hrs") {
													$rate_of_reservation = 5000;
												}
												else if ($hourstay =="24 hrs") {
													$rate_of_reservation = 8000;
												}
										}
										else if($resortname=="Villa Jocris 3 resort")
										{
												if ($hourstay =="12 hrs") {
													$rate_of_reservation = 6000;
												}
												else if ($hourstay =="24 hrs") {
													$rate_of_reservation = 12000;
												}
										}
										else if($resortname=="Villa Leonora resort")
										{
												if ($hourstay =="12 hrs") {
													$rate_of_reservation = 15000;
												}
												else if ($hourstay =="24 hrs") {
													$rate_of_reservation = 25000;
												}
										}

										$totpay = $rate_of_reservation;
										if ($addon=="yes") {
											$ftotpay = $totpay + 200;
										}
										else if ($addon=="no") {
											$ftotpay = $totpay;
										}
		                                
		                                $bal = $ftotpay - $dpay;
										$pay = "INSERT INTO `payment`(`id`,`uname`,`fname`,`lname`,`cnum`,`resortname`,`cin`,`hourstay`,`totpay`,`add on`,`ftotpay`,`deposit`,`balance`,`stat`) VALUES ('$id','$uname','$fname','$lname','$cnum','$resortname','$cin','$hourstay','$totpay','$addon','$ftotpay','$dpay','$bal','$st')";
										$sqll = "INSERT INTO `status`(`id`,`uname`,`fname`,`lname`,`email`,`resortname`,`hourstay`,`addon`,`cnum`,`cin`,`stat`) VALUES ('$id','$uname','$fname','$lname','$email','$resortname','$hourstay','$addon','$cnum','$cin','$st')";
														
										if(mysqli_query($con,$pay)){ 
											if (mysqli_query($con,$sqll)){
										 
										echo "<script type='text/javascript'> alert('Reservation Confirmed')</script>";
										echo "<script type='text/javascript'> window.location=dynamicadmin.php'</script>";
										    }
										}			
								}
							}
							else{

                                $st = $_POST['status'];
										$psqll = "INSERT INTO `status`(`id`,`uname`,`fname`,`lname`,`email`,`resortname`,`hourstay`,`addon`,`cnum`,`cin`,`stat`) VALUES ('$id','$uname','$fname','$lname','$email','$resortname','$hourstay','$addon','$cnum','$cin','$st')";
										if(mysqli_query($con,$psqll))
										{	
										
										echo "<script type='text/javascript'> alert('Reservation Cancelled')</script>";
										echo "<script type='text/javascript'> window.location=dynamicadmin.php'</script>";
				
									}

							}	
						}
?>