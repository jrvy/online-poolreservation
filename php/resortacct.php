 
<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Admin Dashboard</title>
  <link rel="icon"  href="../image/icon/admin.png">

  <!-- Bootstrap core CSS -->
  <link href="css/bootstrap/bootstrap.min.css" rel="stylesheet">
  <!-- MDB BOOTSTRAP -->
  <link rel="stylesheet" type="text/css" href="css/mdb/mdb.min.css">
  <!-- Custom styles for this template -->
  <link href="css/admincss.css" rel="stylesheet">
  <!-- FONT AWESOME -->
  <link rel="stylesheet" type="text/css" href="fonts/font-awesome.css">


</head>

<body>

<!--Navbar -->
<nav class="navbar navbar-expand-lg blue-gradient">
  <a class="navbar-brand black-text" href="#">Pansol Private Pool Reservation</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarDropdownMenuLink-333"
    aria-controls="navbarDropdownMenuLink-333" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarDropdownMenuLink-333">
    <ul class="navbar-nav ml-auto nav-flex-icons">
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle white-text" id="navbarDropdownMenuLink-333" data-toggle="dropdown" aria-haspopup="true"
          aria-expanded="false">Manage
        </a>
        <div class="dropdown-menu dropdown-default" aria-labelledby="navbarDropdownMenuLink-333">
          <a class="dropdown-item" href="logout.php">Log out</a>
        </div>
      </li>
    </ul>
  </div>
</nav>
<!-- Navbar -->
<div class="d-flex" id="wrapper">

    <!-- Sidebar -->
    <div class="elegant-color border-right" id="sidebar-wrapper">
      <div class="list-group list-group-flush">
        <a href="admindashboard.php" class="list-group-item list-group-item-action"><i class="fa fa-dashboard"></i> Dashboard</a>
        <a href="poolbook.php" class="list-group-item list-group-item-action "><i class="fa fa-bookmark"></i> Pool Booking</a>
        <a href="payment.php" class="list-group-item list-group-item-action "><i class="fa fa-money"></i> Payment</a>
        <a href="report.php" class="list-group-item list-group-item-action "><i class="fa fa-bar-chart-o"></i> Reports</a>
        <a href="resortacct.php" class="list-group-item list-group-item-action blue-gradient white-text"><i class="fa fa-user"></i> Resort Account</a>
        <button class='btn btn-primary btn' data-toggle='modal' data-target="#modalLoginForm">Add resort</button>
      </div>
    </div>
    <!-- /#sidebar-wrapper -->
    <div class="container heavy-rain-gradient">
      <div class="row">
        <div class="col-12">
          <div class="card text-center mt-2">
            <div class="card-header success-color">
              RESORT ACCOUNT
            </div>
            
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-striped table-bordered">
                  <thead>
                    <tr>
                      <th class="text-info">#</th>
                      <th class="text-warning">Resort Name</th>
                    </tr>
                   </thead>
                   <tbody>
                      <?php
                      include ('db.php');
                      $tsql = "select * from resortacct";
                      $tre = mysqli_query($con,$tsql);
                      while($trow=mysqli_fetch_array($tre) )

                      echo"<tr>
                      <th>".$trow['id']."</th>
                      <th>".$trow['resortname']."</th>
                      </tr>";
                      ?>
                   </tbody>
                </table>
              </div>
            </div>
            <div class="card-footer text-muted success-color black-text">
              PRIVADO
            </div>
          </div>
        </div>
      </div>
    </div>


    
            
      
    <!-- DEOMO-->
      <div class='panel-body'>
          <div class="modal fade" id="modalLoginForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
            aria-hidden="true">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-header text-center">
                  <h4 class="modal-title w-100 font-weight-bold">Sign in</h4>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body mx-3">
                  <form method='post'>
                  <div class="md-form mb-5">
                    <i class="fa fa-user prefix grey-text"></i>
                    <input type="text" name='usname' value='<?php echo $fname; ?>' class='form-control validate' placeholder='Enter User name'>
                    <label data-error="wrong" data-success="right"></label>
                  </div>

                  <div class="md-form mb-4">
                    <i class="fa fa-lock prefix grey-text"></i>
                    <input type="password" name='pasd' value='<?php echo $ps; ?>' class='form-control validate' placeholder='Enter Password'>
                    <label data-error="wrong" data-success="right"></label>
                  </div>

                </div>
                  <div class='modal-footer'>
                      <button type='button' class='btn btn-default' data-dismiss='modal'>Close</button>
                        <input type='submit' name='up' value='Update' class='btn btn-primary'>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
        <!--DEMO END-->
                <!-- /. ROW  -->
      </div>
</div>

    <!-- JS Scripts-->

    <!-- jQuery-2.2.4 js -->
    <script src="js/jquery/jquery-2.2.4.min.js"></script>
    <!-- Popper js -->
    <script src="js/bootstrap/popper.min.js"></script>
    <!-- Bootstrap-4 js -->
    <script src="js/bootstrap/bootstrap.min.js"></script>

    <!-- SCRIPTS -->
    <!-- JQuery -->
    <script type="text/javascript" src="js/mdb/jquery-3.3.1.min.js"></script>
    <!-- Bootstrap tooltips -->
    <script type="text/javascript" src="js/mdb/popper.min.js"></script>
    <!-- MDB core JavaScript -->
    <script type="text/javascript" src="js/mdb/mdb.js"></script>
    <!-- Bootstrap core JavaScript -->
    <script src="js/adminjs/jquery.min.js"></script>


  <!-- SCRIPTS -->
  


</body>
</html>

