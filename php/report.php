

<!DOCTYPE html>
<html lang="en">
<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Admin Reports Dashboard</title>
    <link rel="icon"  href="../image/icon/admin.png">


	<!-- Bootstrap core CSS -->
    <link href="css/bootstrap/bootstrap.min.css" rel="stylesheet">
    <!-- MDB BOOTSTRAP -->
    <link rel="stylesheet" type="text/css" href="css/mdb/mdb.min.css">
    <!-- Custom styles for this template -->
    <link href="css/admincss.css" rel="stylesheet">
    <!-- FONT AWESOME -->
    <link rel="stylesheet" type="text/css" href="fonts/font-awesome.css">
    
	<link rel="stylesheet" href="assets/css/morris.css">

	<script src="assets/js/jquery.min.js"></script>
    <script src="assets/js//raphael-min.js"></script>
    <script src="assets/js/morris.min.js"></script>
    <!-- Google Fonts-->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />

</head>
<body>

<!--Navbar -->
<nav class="navbar navbar-expand-lg blue-gradient">
  <a class="navbar-brand black-text" href="#">Pansol Private Pool Reservation</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarDropdownMenuLink-333"
    aria-controls="navbarDropdownMenuLink-333" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarDropdownMenuLink-333">
    <ul class="navbar-nav ml-auto nav-flex-icons">
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle white-text" id="navbarDropdownMenuLink-333" data-toggle="dropdown" aria-haspopup="true"
          aria-expanded="false">Manage
        </a>
        <div class="dropdown-menu dropdown-default" aria-labelledby="navbarDropdownMenuLink-333">
          <a class="dropdown-item" href="logout.php">Log out</a>
        </div>
      </li>
    </ul>
  </div>
</nav>
<!-- Navbar -->
<div class="d-flex" id="wrapper">

    <!-- Sidebar -->
    <div class="elegant-color border-right" id="sidebar-wrapper">
      <div class="list-group list-group-flush">
        <a href="admindashboard.php" class="list-group-item list-group-item-action "><i class="fa fa-dashboard"></i> Dashboard</a>
        <a href="poolbook.php" class="list-group-item list-group-item-action "><i class="fa fa-bookmark"></i> Pool Booking</a>
        <a href="payment.php" class="list-group-item list-group-item-action "><i class="fa fa-money"></i> Payment</a>
        <a href="report.php" class="list-group-item list-group-item-action blue-gradient white-text "><i class="fa fa-bar-chart-o"></i> Reports</a>
        <a href="resortacct.php" class="list-group-item list-group-item-action"><i class="fa fa-user"></i> Resort Account</a>
      </div>
    </div>
    <!-- /#sidebar-wrapper -->
    <div class="container heavy-rain-gradient">
      <div class="row">
        <?php 
                include('db.php');
                
                    
                    $query = "SELECT * FROM payment";
                    $result = mysqli_query($con, $query);
                    $chart_data = '';
                    $tot = '';
                    while($row = mysqli_fetch_array($result))
                    {
                     $chart_data .= "{ date:'".$row["cin"]."', profit:".$row["ftotpay"] ."}, ";
                    }
                    $chart_data = substr($chart_data, 0, -2);
                
?>
        <div id="chart"></div>
        <div class="col-12">
          <div class="card text-center mt-2">
            <div class="card-header success-color">
              STATUS OF RESERVATION
            </div>
            <div class="card-body">
              <h5 class="card-title">New Reservation<span class="badge badge-secondary"></span></h5>
              <table class="table table-striped table-bordered">
                <thead>
                  <tr>
                    <th class="text-primary">Id</th>
                    <th class="text-warning">Name</th>
                    <th class="text-warning">Contact Number</th>
                    <th class="text-warning">Resort Name</th>
                    <th class="text-warning">Hourstay</th>
                    <th class="text-warning">Date</th>
                    <th class="text-warning">Rent</th>
                    <th class="text-warning">Add On</th>
                    <th class="text-danger">Gr.Total</th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                    $sql="select * from payment";
                    $re = mysqli_query($con,$sql);
                    while($row = mysqli_fetch_array($re))
                    {
                     $id = $row['id'];
                    if($id % 2 ==1 )
                    {
                        echo"<tr class='gradeC'>
                        <td>".$row['id']." </td>
                        <td>".$row['fname']." ".$row['lname']."</td>
                        <td>".$row['cnum']."</td>
                        <td>".$row['resortname']."</td>
                                                    
                                                    
                        <td>".$row['hourstay']."</td>
                        <td>".$row['cin']."</td>
                        <td>Php ".$row['totpay']."</td>
                        <td>".$row['add on']."</td>
                        <td>Php ".$row['ftotpay']."</td>
                        </tr>";
                    }
                    else
                    {
                        echo"<tr class='gradeU'>
                        <td>".$row['id']." </td>
                        <td>".$row['fname']." ".$row['lname']."</td>
                        <td>".$row['cnum']."</td>
                        <td>".$row['resortname']."</td>
                                                    
                                                    
                        <td>".$row['hourstay']."</td>
                        <td>".$row['cin']."</td>
                        <td>Php".$row['totpay']."</td>
                        <td>".$row['add on']."</td>
                        <td>Php ".$row['ftotpay']."</td>
                        </tr>";
                                            
                    }
                                        
                }
                                        
                                    ?>
                </tbody>
              </table>

            </div>
            <div class="card-footer text-muted success-color black-text">
              PRIVADO
            </div>
          </div>
        </div>
      </div>
    </div>    

    <!-- JS Scripts-->

    <!-- jQuery-2.2.4 js -->
    <script src="js/jquery/jquery-2.2.4.min.js"></script>
    <!-- Popper js -->
    <script src="js/bootstrap/popper.min.js"></script>
    <!-- Bootstrap-4 js -->
    <script src="js/bootstrap/bootstrap.min.js"></script>

    <!-- SCRIPTS -->
    <!-- JQuery -->
    <script type="text/javascript" src="js/mdb/jquery-3.3.1.min.js"></script>
    <!-- Bootstrap tooltips -->
    <script type="text/javascript" src="js/mdb/popper.min.js"></script>
    <!-- MDB core JavaScript -->
    <script type="text/javascript" src="js/mdb/mdb.js"></script>
    <!-- Bootstrap core JavaScript -->
    <script src="js/adminjs/jquery.min.js"></script>


    <!-- Metis Menu Js -->
    <script src="assets/js/jquery.metisMenu.js"></script>
    <!-- Morris Chart Js -->
    <script src="assets/js/morris/raphael-2.1.0.min.js"></script>
    <script src="assets/js/morris/morris.js"></script>
    <!-- Custom Js -->
    <script src="assets/js/custom-scripts.js"></script>
 
   
</body>
</html>
<script>
Morris.Line({
 element : 'chart',
 data:[<?php echo $chart_data; ?>],
 xkey:'date',
 ykeys:['profit'],
 labels:['Profit'],
 hideHover:'auto',
 stacked:true
});
</script>