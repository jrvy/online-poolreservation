<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Admin Dashboard</title>
  <link rel="icon"  href="../image/icon/admin.png">

  <!-- Bootstrap core CSS -->
  <link href="css/bootstrap/bootstrap.min.css" rel="stylesheet">
  <!-- MDB BOOTSTRAP -->
  <link rel="stylesheet" type="text/css" href="css/mdb/mdb.min.css">
  <!-- Custom styles for this template -->
  <link href="css/admincss.css" rel="stylesheet">
  <!-- FONT AWESOME -->
  <link rel="stylesheet" type="text/css" href="fonts/font-awesome.css">


</head>

<body>

<!--Navbar -->
<nav class="navbar navbar-expand-lg blue-gradient">
  <a class="navbar-brand black-text" href="#">Pansol Private Pool Reservation</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarDropdownMenuLink-333"
    aria-controls="navbarDropdownMenuLink-333" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarDropdownMenuLink-333">
    <ul class="navbar-nav ml-auto nav-flex-icons">
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle white-text" id="navbarDropdownMenuLink-333" data-toggle="dropdown" aria-haspopup="true"
          aria-expanded="false">Manage
        </a>
        <div class="dropdown-menu dropdown-default" aria-labelledby="navbarDropdownMenuLink-333">
          <a class="dropdown-item" href="logout.php">Log out</a>
        </div>
      </li>
    </ul>
  </div>
</nav>
<!-- Navbar -->
<div class="d-flex" id="wrapper">

    <!-- Sidebar -->
    <div class="elegant-color border-right" id="sidebar-wrapper">
      <div class="list-group list-group-flush">
        <a href="admindashboard.php" class="list-group-item list-group-item-action"><i class="fa fa-dashboard"></i> Dashboard</a>
        <a href="poolbook.php" class="list-group-item list-group-item-action "><i class="fa fa-bookmark"></i> Pool Booking</a>
        <a href="payment.php" class="list-group-item list-group-item-action "><i class="fa fa-money"></i> Payment</a>
        <a href="report.php" class="list-group-item list-group-item-action "><i class="fa fa-bar-chart-o"></i> Reports</a>
        <a href="resortacct.php" class="list-group-item list-group-item-action "><i class="fa fa-user"></i> Resort Account</a>
      </div>
    </div>
    <!-- /#sidebar-wrapper -->
    <div class="container heavy-rain-gradient">
      <div class="row">
        <div class="col-12">
          <div class="card text-center mt-2">
            <div class="card-header success-color">
              ADD NEW RESORT
            </div>
            <form method="post" enctype="multipart/form-data">
            <div class="card-body">
              <h5 class="card-title">New Resort</h5>
              <div class="row"><!-- row -->
                <div class="col-md-4 mb-4">
                  <div class="md-form">
                    <textarea id="usname" name="usname" class="md-textarea form-control" required></textarea>
                    <label for="usname" class="text-danger">Username</label>
                  </div>
                </div> <!-- col-md-6 -->
                <div class="col-md-4 mt-4">
                  <div class="md-form">
                    <label for="pass"  class="text-danger">Password</label>
                    <input type="password" id="pass" name="pass2" class="form-control" required autocomplete="off">
                  </div>
                </div> <!-- col-md-6 -->
                <div class="col-md-4 mt-4">
                  <div class="md-form">
                    <label for="pass"  class="text-danger">Confirm password</label>
                    <input type="password" id="pass2" name="pass" class="form-control" required autocomplete="off">
                  </div>
                </div> <!-- col-md-6 -->
              </div><!-- row -->
              <div class="row"><!-- row -->
                <div class="col-md-3">
                  <div class="md-form">
                    <textarea id="rname" name="rname" class="md-textarea form-control" required></textarea>
                    <label for="rname" class="text-danger">Resort Name</label>
                  </div>
                </div> <!-- col-md-4 -->
                <div class="col-md-3">
                  <div class="md-form">
                    <textarea id="loc" name="loc" class="md-textarea form-control" required></textarea>
                    <label for="loc" class="text-danger">Location</label>
                  </div>
                </div> <!-- col-md-4 -->
                <div class="col-md-3">
                  <div class="md-form">
                    <textarea id="cnum" name="cnum" class="md-textarea form-control" required></textarea>
                    <label for="cnum" class="text-danger">Contact number</label>
                  </div>
                </div> <!-- col-md-4 -->
                <div class="col-md-3">
                  <label class="mt-4 text-danger">Type of resort</label>
                    <div class="md-form">
                        <select class="form-control" name="type" required>
                          <option value selected></option>
                          <option value="Family resort">Family resort</option>
                          <option value="Debut, wedding type resort">Debut, wedding type resort</option>
                        </select>
                    </div>
                </div> <!-- col-md-4 -->
              </div><!-- row -->
              <div class="row"><!-- row -->
                <div class="col-md-6">
                  <div class="md-form">
                    <textarea id="r12" name="r12" class="md-textarea form-control" required></textarea>
                    <label for="r12" class="text-danger">Rent for 12 hours</label>
                  </div>
                </div> <!-- col-md-6 -->
                <div class="col-md-6">
                  <div class="md-form">
                    <textarea id="r24" name="r24" class="md-textarea form-control" required></textarea>
                    <label for="r24" class="text-danger">Rent for 24 hours</label>
                  </div>
                </div> <!-- col-md-6 -->
              </div><!-- row -->
              <div class="row"><!-- row -->
                <div class="col-md-4">
                  <input type="hidden" name="size" value="1000000">
                    <div>
                      <label class="text-danger">Picture of pool</label>
                      <input type="file" name="ppool" required>
                    </div>
                </div> <!-- col-md-4 -->
                <div class="col-md-4">
                  <input type="hidden" name="size" value="1000000">
                    <div>
                      <label class="text-danger">Picture of room</label>
                      <input type="file" name="proom" required multiple>
                    </div>
                </div> <!-- col-md-4 -->
                <div class="col-md-4">
                  <input type="hidden" name="size" value="1000000">
                    <div>
                      <label class="text-danger">Picture of resort</label>
                      <input type="file" name="pgen" required multiple>
                    </div>
                </div> <!-- col-md-4 -->
              </div><!-- row -->
              <hr>
              <div class="row"><!-- row -->
                <div class="col-md-3">
                  <label class="mt-4 text-danger">How many ordinary rooms?</label>
                    <div class="md-form">
                        <select class="form-control" name="rmord" required>
                          <option value selected></option>
                          <option value="1">1</option>
                          <option value="2">2</option>
                          <option value="3">3</option>
                          <option value="4">4</option>
                          <option value="5">5</option>
                          <option value="6">6</option>
                          <option value="7">7</option>
                          <option value="8">8</option>
                          <option value="9">9</option>
                          <option value="10">10</option>
                        </select>
                    </div>
                </div> <!-- col-md-4 -->
                <div class="col-md-3">
                  <label class="mt-4 text-danger">How many airconditioned rooms?</label>
                    <div class="md-form">
                        <select class="form-control" name="rmair" required>
                          <option value selected></option>
                          <option value="1">1</option>
                          <option value="2">2</option>
                          <option value="3">3</option>
                          <option value="4">4</option>
                          <option value="5">5</option>
                          <option value="6">6</option>
                          <option value="7">7</option>
                          <option value="8">8</option>
                          <option value="9">9</option>
                          <option value="10">10</option>
                        </select>
                    </div>
                </div> <!-- col-md-4 -->
                <div class="col-md-3">
                  <label class="mt-4 text-danger">Kiddie pool Depth</label>
                    <div class="md-form">
                        <select class="form-control" name="pldepthkiddie" required>
                          <option value selected></option>
                          <option value="2ft">2ft</option>
                          <option value="3ft">3ft</option>
                          <option value="4ft">4ft</option>
                        </select>
                    </div>
                </div> <!-- col-md-4 -->
                <div class="col-md-3">
                  <label class="mt-4 text-danger">Adult pool Depth</label>
                    <div class="md-form">
                        <select class="form-control" name="pldepthadult" required>
                          <option value selected></option>
                          <option value="4ft">4ft</option>
                          <option value="5ft">5ft</option>
                          <option value="6ft">6ft</option>
                        </select>
                    </div>
                  </div>
                </div> <!-- col-md-4 -->
              </div><!-- row -->
              <div class="row"><!-- row -->
                
                <div class="col-md-3">
                  <legend class="text-center text-danger">Includes</legend>
                  <div class="form-check">
                      <input type="checkbox" class="form-check-input"  name="inckaraoke" value="Karaoke">
                      <label class="form-check-label">Karaoke</label>
                  </div>
                  <div class="form-check">
                      <input type="checkbox" class="form-check-input"  name="incbilliards" value="Biliards">
                      <label class="form-check-label">Biliards</label>
                  </div>
                  <div class="form-check">
                      <input type="checkbox" class="form-check-input" id="" name="gasul" value="Gasul(propane)">
                      <label class="form-check-label">Gasul</label>
                  </div>
                </div> <!-- col-md-3-->
                <div class="col-md-3">
                  <label class="mt-4 text-danger">Number of Kiddie pool</label>
                  <div class="md-form">
                    <select class="form-control" name="plkiddie" required>
                          <option value selected></option>
                          <option value="1">1</option>
                          <option value="2">2</option>
                          <option value="3">3</option>
                    </select>
                  </div>
                </div> <!-- col-md-3 -->
                <div class="col-md-3">
                  <label class="mt-4 text-danger">Number of Adult pool</label>
                  <div class="md-form">
                    <select class="form-control" name="pladult" required>
                          <option value selected></option>
                          <option value="1">1</option>
                          <option value="2">2</option>
                          <option value="3">3</option>
                    </select>
                  </div>
                </div> <!-- col-md-3 -->
                <div class="col-md-3">
                  <div class="md-form mr-2">
                    <textarea id="maxper" name="maxper" class="md-textarea form-control" required></textarea>
                    <label for="maxper" class="text-danger"><strong>Maximun persons</strong></label>
                  </div>
                </div> <!-- col-md-3 -->
              </div><!-- row -->
              <input type="submit" name="submit" value="ADD RESORT" class="btn btn-success">
            </div>
            </form>
            <div class="card-footer text-muted success-color black-text text-center">
              BRGY PANSOL ONLINE PRIVATE POOL RESERVATION
            </div>
          </div>
        </div>
      </div>
    </div>
<?php
            if(isset($_POST['submit']))
              {
                    // Get pool image name
                    $image = $_FILES['ppool']['name'];
                    // Get room image name
                    $image1 = $_FILES['proom']['name'];
                    // Get general image name
                    $image2 = $_FILES['proom']['name'];
                
                    // image file directory
                    $target = "uploads/pool/".basename($image);
                    // image file directory
                    $target1 = "uploads/room/".basename($image1);
                    // image file directory
                    $target2 = "uploads/general/".basename($image2);

                    include('db.php');
                    $new ="Resort";
                    $newResort="INSERT INTO resortmng(`rname`,`loc`,`type`,`cnum`,`r12`,`r24`,`ppool`,`proom`,`pgen`,`rmord`,`rmair`,`pldepthkiddie`,`pldepthadult`,`plkiddie`,`pladult`,`inckaraoke`,`incbilliards`,`gasul`,`maxper`) VALUES ('$_POST[rname]','$_POST[loc]','$_POST[type]','$_POST[cnum]','$_POST[r12]','$_POST[r24]','$image','$image1','$image2','$_POST[rmord]','$_POST[rmair]','$_POST[pldepthkiddie]','$_POST[pldepthadult]','$_POST[plkiddie]','$_POST[pladult]','$_POST[inckaraoke]','$_POST[incbilliards]','$_POST[gasul]','$_POST[maxper]')";


                    $newuser="INSERT INTO login(`usname`,`pass`,`class`) VALUES ('$_POST[usname]','$_POST[pass]','$new')";
                    if( (mysqli_query($con,$newResort)) and (mysqli_query($con,$newuser)))
                    {
                      if (move_uploaded_file($_FILES['ppool']['tmp_name'], $target) and move_uploaded_file($_FILES['proom']['tmp_name'], $target1) and move_uploaded_file($_FILES['pgen']['tmp_name'], $target2)){
                      echo "<script type='text/javascript'> alert('Succesfully added')</script>";
                      }
                    }
                    else
                    {
                      echo "<script type='text/javascript'> alert('Error adding')</script>";
                      
                    }

              }

?> 

 


    <!-- JS Scripts-->

    <!-- Js Validate -->
    <script src="../bootstrap-validate-master/dist/bootstrap-validate.js"></script>
    <!-- jQuery-2.2.4 js -->
    <script src="js/jquery/jquery-2.2.4.min.js"></script>
    <!-- Popper js -->
    <script src="js/bootstrap/popper.min.js"></script>
    <!-- Bootstrap-4 js -->
    <script src="js/bootstrap/bootstrap.min.js"></script>

    <!-- SCRIPTS -->
    <!-- JQuery -->
    <script type="text/javascript" src="js/mdb/jquery-3.3.1.min.js"></script>
    <!-- Bootstrap tooltips -->
    <script type="text/javascript" src="js/mdb/popper.min.js"></script>
    <!-- MDB core JavaScript -->
    <script type="text/javascript" src="js/mdb/mdb.js"></script>
    <!-- Bootstrap core JavaScript -->
    <script src="js/adminjs/jquery.min.js"></script>


  <!-- SCRIPTS -->
  

  <script>
  
    bootstrapValidate('#cnum' ,'numeric:You can only input numeric values! |startsWith:09:Your phone number needs to start with 09 |max:11:Max.num is 11 digits');
    bootstrapValidate(['#pass','#r12','#r24','#maxper'],'numeric:You can only input numeric values!');
    bootstrapValidate('#loc','#usname','#rname' ,'alphanumeric:');
    bootstrapValidate('#pass2', 'matches:#pass:Your password does not match');
    </script>


</body>
</html>

