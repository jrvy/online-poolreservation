-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 23, 2020 at 03:07 AM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.2.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `resort`
--

-- --------------------------------------------------------

--
-- Table structure for table `cancel`
--

CREATE TABLE `cancel` (
  `id` int(50) NOT NULL,
  `fname` varchar(255) DEFAULT NULL,
  `lname` varchar(255) DEFAULT NULL,
  `cnum` text,
  `resortname` varchar(255) DEFAULT NULL,
  `cin` date DEFAULT NULL,
  `stat` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cancel`
--

INSERT INTO `cancel` (`id`, `fname`, `lname`, `cnum`, `resortname`, `cin`, `stat`) VALUES
(20, 'harvey', 'pua', '09350551580', 'Alegria resort', '2019-03-31', 'Submit'),
(21, 'harvey', 'pua', '09350551580', 'Alegria resort', '2019-04-09', 'Not confirm');

-- --------------------------------------------------------

--
-- Table structure for table `image_upload`
--

CREATE TABLE `image_upload` (
  `id` int(11) NOT NULL,
  `image` varchar(255) NOT NULL,
  `image_text` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `image_upload`
--

INSERT INTO `image_upload` (`id`, `image`, `image_text`) VALUES
(0, 'palmtree.png', '');

-- --------------------------------------------------------

--
-- Table structure for table `login`
--

CREATE TABLE `login` (
  `id` int(10) NOT NULL,
  `usname` varchar(30) DEFAULT NULL,
  `pass` varchar(30) DEFAULT NULL,
  `class` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `login`
--

INSERT INTO `login` (`id`, `usname`, `pass`, `class`) VALUES
(1, 'admin', '12345', NULL),
(2, 'Alegria resort', '12345', 'Resort'),
(3, 'Carlos', '12345', 'Resort'),
(4, 'ELW ', '12345', 'Resort'),
(5, 'laguna blue', '12345', 'Resort'),
(6, 'lenzen', '12345', 'Resort'),
(7, 'rubi rosa', '12345', 'Resort'),
(8, 'vergara', '12345', 'Resort'),
(9, 'jocris 1', '12345', 'Resort'),
(10, 'jocris 2', '12345', 'Resort'),
(11, 'jocris 3', '12345', 'Resort'),
(12, 'santiago medina', '12345', 'Resort'),
(13, 'm and a', '12345', 'Resort'),
(14, 'Mary Grace', '12345', 'Resort'),
(15, 'la azu thea', '12345', 'Resort'),
(16, 'villa carmecita', '12345', 'Resort'),
(17, 'inlg', '12345', 'Resort'),
(18, 'valerie', '12345', 'Resort'),
(19, 'kayrense', '12345', 'Resort'),
(20, 'floredeliza', '12345', 'Resort'),
(21, 'leonora', '12345', 'Resort'),
(22, 'wapu', '12345', 'Resort');

-- --------------------------------------------------------

--
-- Table structure for table `payment`
--

CREATE TABLE `payment` (
  `id` int(50) NOT NULL,
  `uname` varchar(255) DEFAULT NULL,
  `fname` varchar(100) DEFAULT NULL,
  `lname` varchar(100) DEFAULT NULL,
  `cnum` text NOT NULL,
  `resortname` varchar(255) DEFAULT NULL,
  `cin` date DEFAULT NULL,
  `hourstay` varchar(20) DEFAULT NULL,
  `totpay` int(50) DEFAULT NULL,
  `add on` varchar(20) DEFAULT NULL,
  `ftotpay` int(100) DEFAULT NULL,
  `deposit` int(50) DEFAULT NULL,
  `balance` int(50) DEFAULT NULL,
  `stat` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `payment`
--

INSERT INTO `payment` (`id`, `uname`, `fname`, `lname`, `cnum`, `resortname`, `cin`, `hourstay`, `totpay`, `add on`, `ftotpay`, `deposit`, `balance`, `stat`) VALUES
(33, 'harvey', 'harvey', 'pua', '09350551580', 'Carlos resort', '2019-04-23', '24 hrs', 5000, 'yes', 5200, 500, 4700, 'Confirmed');

-- --------------------------------------------------------

--
-- Table structure for table `poolreservation`
--

CREATE TABLE `poolreservation` (
  `id` int(50) NOT NULL,
  `uname` varchar(255) NOT NULL,
  `fname` varchar(255) NOT NULL,
  `mname` varchar(255) DEFAULT NULL,
  `lname` varchar(255) NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `resortname` varchar(255) DEFAULT NULL,
  `hourstay` varchar(20) DEFAULT NULL,
  `mor` varchar(255) DEFAULT NULL,
  `cnum` text NOT NULL,
  `add on` varchar(20) DEFAULT NULL,
  `cin` date DEFAULT NULL,
  `stat` varchar(15) DEFAULT NULL,
  `image` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `poolreservation`
--

INSERT INTO `poolreservation` (`id`, `uname`, `fname`, `mname`, `lname`, `email`, `resortname`, `hourstay`, `mor`, `cnum`, `add on`, `cin`, `stat`, `image`) VALUES
(33, 'harvey', 'harvey', 'a', 'pua', 'puajohnharvey@gmail.com', 'Alegria resort', '24 hrs', '', '09350551580', 'yes', '2019-04-09', 'Confirmed', '5c8fd29d4e6499.60512581.jpg'),
(34, 'harvey', 'harvey', 'a', 'pua', 'puajohnharvey@gmail.com', 'Alegria resort', '12 hrs', 'day', '09350551580', 'yes', '2019-04-09', 'Not Confirm', 'palmtree.png'),
(39, 'harvey', 'harvey', 'a', 'pua', 'puajohnharvey@gmail.com', 'Alegria resort', '12 hrs', 'night', '09350551580', 'yes', '2019-04-10', 'Not Confirm', 'palmtree.png'),
(40, 'harvey', 'harvey', 'a', 'pua', 'puajohnharvey@gmail.com', 'Alegria resort', '12 hrs', 'night', '09350551580', 'yes', '2019-04-11', 'Not Confirm', '5c8fd2bfc60105.45712130.jpg'),
(41, 'harvey', 'harvey', 'a', 'pua', 'puajohnharvey@gmail.com', 'Alegria resort', '12 hrs', 'night', '09350551580', 'yes', '2019-04-24', 'Not Confirm', 'sydney.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `resortacct`
--

CREATE TABLE `resortacct` (
  `id` int(11) NOT NULL,
  `resortname` varchar(255) DEFAULT NULL,
  `cnum` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `resortacct`
--

INSERT INTO `resortacct` (`id`, `resortname`, `cnum`) VALUES
(1, 'Alegria resort', ''),
(2, 'Carlos resort', ''),
(3, 'Elw resort', ''),
(4, 'Laguna blue resort', ''),
(5, 'Lenzen Azy resort', ''),
(6, 'Rubi Rosa resort', ''),
(7, 'Vergara resort', ''),
(8, 'Villa Jocris 1 resort', ''),
(9, 'Villa Jocris 2 resort', ''),
(10, 'Santiago Medina resort', ''),
(11, 'M and A resort', ''),
(12, 'Mary Grace resort', ''),
(13, 'La Azu Thea resort', ''),
(14, 'Villa Carmencita resort', ''),
(15, 'INLG resort', ''),
(16, 'Valerie resort', ''),
(17, 'Kayrense resort', ''),
(18, 'Floredeliza resort', ''),
(19, 'Villa Leonora resort', ''),
(20, 'Villa Jocris 3 resort', '');

-- --------------------------------------------------------

--
-- Table structure for table `resortmng`
--

CREATE TABLE `resortmng` (
  `id` int(11) NOT NULL,
  `rname` varchar(255) DEFAULT NULL,
  `loc` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `cnum` varchar(255) DEFAULT NULL,
  `r12` varchar(50) DEFAULT NULL,
  `r24` varchar(50) DEFAULT NULL,
  `ppool` varchar(255) DEFAULT NULL,
  `proom` varchar(255) DEFAULT NULL,
  `pgen` varchar(255) DEFAULT NULL,
  `rmord` varchar(10) DEFAULT NULL,
  `rmair` varchar(10) DEFAULT NULL,
  `pldepthkiddie` varchar(50) DEFAULT NULL,
  `pldepthadult` varchar(50) DEFAULT NULL,
  `plkiddie` varchar(50) DEFAULT NULL,
  `pladult` varchar(50) DEFAULT NULL,
  `inckaraoke` varchar(50) DEFAULT NULL,
  `incbilliards` varchar(50) DEFAULT NULL,
  `gasul` varchar(50) DEFAULT NULL,
  `maxper` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `resortmng`
--

INSERT INTO `resortmng` (`id`, `rname`, `loc`, `type`, `cnum`, `r12`, `r24`, `ppool`, `proom`, `pgen`, `rmord`, `rmair`, `pldepthkiddie`, `pldepthadult`, `plkiddie`, `pladult`, `inckaraoke`, `incbilliards`, `gasul`, `maxper`) VALUES
(4, 'Alegria Resort', 'Miramonte Village', 'Family resort', '09350551580', '12,000', '24,000', '5c8fd2bfc60105.45712130.jpg', '5c8fd29d4e6499.60512581.jpg', 'palmtree.png', '3', '4', '2ft', '4ft', '2', '1', 'on', 'on', 'on', '25'),
(5, 'Carlos Resort', 'Pansol Calamba City', 'Debut, wedding type resort', '09350667856', '3,500', '11,000', '5c8fd2bfc60105.45712130.jpg', '5c8fd29d4e6499.60512581.jpg', 'palmtree.png', '3', '6', '3ft', '4ft', '2', '2', 'on', '', 'on', '30'),
(6, 'Wapu Resort', 'Brgy pansol', 'Family resort', '09235677729', '22222', '10000', '5c8fd2bfc60105.45712130.jpg', '5c8fd29d4e6499.60512581.jpg', '5c8fd29d4e6499.60512581.jpg', '2', '3', '4ft', '5ft', '1', '2', 'Karaoke', 'Biliards', 'Gasul(propane)', '24');

-- --------------------------------------------------------

--
-- Table structure for table `status`
--

CREATE TABLE `status` (
  `id` int(11) NOT NULL,
  `uname` varchar(255) DEFAULT NULL,
  `fname` varchar(255) DEFAULT NULL,
  `lname` varchar(255) DEFAULT NULL,
  `email` text,
  `resortname` varchar(255) DEFAULT NULL,
  `hourstay` varchar(255) DEFAULT NULL,
  `addon` varchar(255) DEFAULT NULL,
  `cnum` varchar(255) DEFAULT NULL,
  `cin` varchar(255) DEFAULT NULL,
  `stat` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `status`
--

INSERT INTO `status` (`id`, `uname`, `fname`, `lname`, `email`, `resortname`, `hourstay`, `addon`, `cnum`, `cin`, `stat`) VALUES
(33, 'harvey', 'harvey', 'pua', 'puajohnharvey@gmail.com', 'Carlos resort', '24 hrs', 'yes', '09350551580', '2019-04-23', 'Confirmed');

-- --------------------------------------------------------

--
-- Table structure for table `trans`
--

CREATE TABLE `trans` (
  `id` int(11) NOT NULL,
  `uname` varchar(50) NOT NULL,
  `fname` varchar(255) NOT NULL,
  `mname` varchar(255) NOT NULL,
  `lname` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `resortname` varchar(255) NOT NULL,
  `hourstay` varchar(255) NOT NULL,
  `mor` varchar(255) DEFAULT NULL,
  `cnum` text NOT NULL,
  `add on` varchar(30) NOT NULL,
  `cin` date NOT NULL,
  `stat` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `trans`
--

INSERT INTO `trans` (`id`, `uname`, `fname`, `mname`, `lname`, `email`, `resortname`, `hourstay`, `mor`, `cnum`, `add on`, `cin`, `stat`, `image`) VALUES
(1, 'harvey', 'harvey', 'a', 'pua', 'puajohnharvey@gmail.com', '5,000 Php', '12 hrs', NULL, '09350551580', 'yes', '2019-03-31', 'Not Confirm', 'palmtree.png'),
(2, 'harvey', 'harvey', 'a', 'pua', 'puajohnharvey@gmail.com', 'Alegria resort', '12 hrs', NULL, '09350551580', 'no', '2019-03-31', 'Not Confirm', '5c8fd2bfc60105.45712130.jpg'),
(3, 'harvey', 'harvey', 'a', 'pua', 'puajohnharvey@gmail.com', 'Carlos resort', '24 hrs', NULL, '09350551580', 'yes', '2019-03-31', 'Not Confirm', 'palmtree.png'),
(4, 'harvey', 'harvey', 'a', 'pua', 'puajohnharvey@gmail.com', 'Carlos resort', '12 hrs', NULL, '09350551580', 'yes', '2019-03-31', 'Not Confirm', '5c8fd29d4e6499.60512581.jpg'),
(5, 'harvey', 'harvey', 'a', 'pua', 'puajohnharvey@gmail.com', 'Laguna blue resort', '24 hrs', NULL, '09350551580', 'yes', '2019-03-31', 'Not Confirm', '5c8fd29d4e6499.60512581.jpg'),
(6, 'harvey', 'harvey', 'a', 'pua', 'puajohnharvey@gmail.com', 'Alegria resort', '12 hrs', NULL, '09350551580', 'yes', '2019-03-31', 'Not Confirm', '5c8fd2bfc60105.45712130.jpg'),
(7, 'harvey', 'harvey', 'a', 'pua', 'puajohnharvey@gmail.com', 'Alegria resort', '12 hrs', NULL, '09350551580', 'yes', '2019-03-31', 'Not Confirm', '5c8fd2bfc60105.45712130.jpg'),
(8, 'harvey', 'harvey', 'a', 'pua', 'puajohnharvey@gmail.com', 'Alegria resort', '12 hrs', NULL, '09350551580', 'yes', '2019-03-31', 'Not Confirm', '5c8fd2bfc60105.45712130.jpg'),
(9, 'harvey', 'harvey', 'a', 'pua', 'puajohnharvey@gmail.com', 'Alegria resort', '12 hrs', NULL, '09350551580', 'yes', '2019-03-31', 'Not Confirm', '5c8fd29d4e6499.60512581.jpg'),
(10, 'harvey', 'harvey', 'a', 'pua', 'puajohnharvey@gmail.com', 'Alegria resort', '12 hrs', NULL, '09350551580', 'no', '2019-03-31', 'Not Confirm', 'palmtree.png'),
(11, 'harvey', 'harvey', 'a', 'pua', 'puajohnharvey@gmail.com', 'Alegria resort', '12 hrs', NULL, '09350551580', 'yes', '2019-04-09', 'Not Confirm', '5c8fd2bfc60105.45712130.jpg'),
(12, 'harvey', 'harvey', 'a', 'pua', 'puajohnharvey@gmail.com', 'Alegria resort', '12 hrs', NULL, '09350551580', 'yes', '2019-04-16', 'Not Confirm', 'palmtree.png'),
(13, 'harvey', 'harvey', 'a', 'pua', 'puajohnharvey@gmail.com', 'Alegria resort', '12 hrs', NULL, '09350551580', 'yes', '2019-04-22', 'Not Confirm', '5c8fd29d4e6499.60512581.jpg'),
(14, 'harvey', 'harvey', 'a', 'pua', 'puajohnharvey@gmail.com', 'Alegria resort', '24 hrs', NULL, '09350551580', 'no', '2019-04-09', 'Not Confirm', 'palmtree.png'),
(15, 'harvey', 'harvey', 'a', 'pua', 'puajohnharvey@gmail.com', 'Alegria resort', '12 hrs', NULL, '09350551580', 'no', '2019-04-24', 'Not Confirm', 'palmtree.png'),
(16, 'mahalkoaics', 'josh', 'b', 'jacinto', 'joshjacinto@gmail.com', 'Kayrense resort', '24 hrs', NULL, '+639154752974', 'yes', '2019-04-22', 'Not Confirm', 'palmtree.png'),
(17, 'harvey', 'harvey', 'a', 'pua', 'puajohnharvey@gmail.com', 'Alegria resort', '12 hrs', NULL, '09350551580', 'yes', '2019-04-09', 'Not Confirm', '5c8fd2bfc60105.45712130.jpg'),
(18, 'harvey', 'harvey', 'a', 'pua', 'puajohnharvey@gmail.com', 'Alegria resort', '24 hrs', NULL, '09350551580', 'yes', '2019-04-23', 'Not Confirm', '5c8fd2bfc60105.45712130.jpg'),
(19, 'harvey', 'harvey', 'a', 'pua', 'puajohnharvey@gmail.com', 'Alegria resort', '12 hrs', NULL, '09350551580', 'yes', '2019-04-15', 'Not Confirm', '5c8fd2bfc60105.45712130.jpg'),
(20, 'harvey', 'harvey', 'a', 'pua', 'puajohnharvey@gmail.com', 'Alegria resort', '24 hrs', NULL, '09350551580', 'yes', '2019-04-10', 'Not Confirm', '5c8fd2bfc60105.45712130.jpg'),
(21, 'harvey', 'harvey', 'a', 'pua', 'puajohnharvey@gmail.com', 'Alegria resort', '12 hrs', NULL, '09350551580', 'yes', '2019-04-08', 'Not Confirm', '5c8fd2bfc60105.45712130.jpg'),
(22, 'harvey', 'harvey', 'a', 'pua', 'puajohnharvey@gmail.com', 'Alegria resort', '12 hrs', NULL, '09350551580', 'yes', '2019-04-30', 'Not Confirm', '5c8fd29d4e6499.60512581.jpg'),
(23, 'harvey', 'harvey', 'a', 'pua', 'puajohnharvey@gmail.com', 'Carlos resort', '24 hrs', NULL, '09350551580', 'yes', '2019-04-23', 'Not Confirm', '5c8fd29d4e6499.60512581.jpg'),
(24, 'harvey', 'harvey', 'a', 'pua', 'puajohnharvey@gmail.com', 'Alegria resort', '12 hrs', NULL, '09350551580', 'yes', '2019-04-08', 'Not Confirm', 'palmtree.png'),
(25, 'harvey', 'harvey', 'a', 'pua', 'puajohnharvey@gmail.com', 'Alegria resort', '<br />\r\n<b>Notice</b>:  Undefined variable: hour in <b>C:xampphtdocsprojOnline Private Pool\reservation.php</b> on line <b>157</b><br />\r\n', 'night', '09350551580', 'yes', '2019-04-10', 'Not Confirm', 'palmtree.png'),
(26, 'harvey', 'harvey', 'a', 'pua', 'puajohnharvey@gmail.com', '<br />\r\n<b>Notice</b>:  Undefined index: res in <b>C:xampphtdocsprojOnline Private Pool\reservation.php</b> on line <b>143</b><br />\r\n', '<br />\r\n<b>Notice</b>:  Undefined variable: hourstays in <b>C:xampphtdocsprojOnline Private Pool\reservation.php</b> on line <b>159</b><br />\r\n', '<br />\r\n<b>Notice</b>:  Undefined variable: day in <b>C:xampphtdocsprojOnline Private Pool\reservation.php</b> on line <b>162</b><br />\r\n', '09350551580', 'yes', '0000-00-00', 'Not Confirm', 'palmtree.png'),
(27, 'harvey', 'harvey', 'a', 'pua', 'puajohnharvey@gmail.com', '<br />\r\n<b>Notice</b>:  Undefined variable: resorts in <b>C:xampphtdocsprojOnline Private Pool\reservation.php</b> on line <b>143</b><br />\r\n', '<br />\r\n<b>Notice</b>:  Undefined variable: hourstays in <b>C:xampphtdocsprojOnline Private Pool\reservation.php</b> on line <b>159</b><br />\r\n', '<br />\r\n<b>Notice</b>:  Undefined variable: day in <b>C:xampphtdocsprojOnline Private Pool\reservation.php</b> on line <b>162</b><br />\r\n', '09350551580', 'yes', '0000-00-00', 'Not Confirm', 'palmtree.png'),
(28, 'harvey', 'harvey', 'a', 'pua', 'puajohnharvey@gmail.com', '<br />\r\n<b>Notice</b>:  Undefined variable: resorts in <b>C:xampphtdocsprojOnline Private Pool\reservation.php</b> on line <b>144</b><br />\r\n', '<br />\r\n<b>Notice</b>:  Undefined variable: hourstays in <b>C:xampphtdocsprojOnline Private Pool\reservation.php</b> on line <b>160</b><br />\r\n', '<br />\r\n<b>Notice</b>:  Undefined variable: day in <b>C:xampphtdocsprojOnline Private Pool\reservation.php</b> on line <b>163</b><br />\r\n', '09350551580', 'yes', '0000-00-00', 'Not Confirm', 'palmtree.png'),
(29, 'harvey', 'harvey', 'a', 'pua', 'puajohnharvey@gmail.com', '', '<br />\r\n<b>Notice</b>:  Undefined variable: hourstays in <b>C:xampphtdocsprojOnline Private Pool\reservation.php</b> on line <b>166</b><br />\r\n', '<br />\r\n<b>Notice</b>:  Undefined variable: day in <b>C:xampphtdocsprojOnline Private Pool\reservation.php</b> on line <b>169</b><br />\r\n', '09350551580', 'yes', '0000-00-00', 'Not Confirm', '5c8fd2bfc60105.45712130.jpg'),
(30, 'harvey', 'harvey', 'a', 'pua', 'puajohnharvey@gmail.com', 'Alegria resort', '12 hrs', 'night', '09350551580', 'yes', '2019-04-10', 'Not Confirm', 'palmtree.png'),
(31, 'harvey', 'harvey', 'a', 'pua', 'puajohnharvey@gmail.com', 'Alegria resort', '12 hrs', 'night', '09350551580', 'yes', '2019-04-11', 'Not Confirm', '5c8fd2bfc60105.45712130.jpg'),
(32, 'harvey', 'harvey', 'a', 'pua', 'puajohnharvey@gmail.com', 'Alegria resort', '12 hrs', 'night', '09350551580', 'yes', '2019-04-24', 'Not Confirm', 'sydney.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `useracct`
--

CREATE TABLE `useracct` (
  `id` int(11) NOT NULL,
  `uname` varchar(255) DEFAULT NULL,
  `fname` varchar(255) DEFAULT NULL,
  `mname` varchar(10) DEFAULT NULL,
  `lname` varchar(255) DEFAULT NULL,
  `cnum` varchar(255) DEFAULT NULL,
  `email` text NOT NULL,
  `password` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `useracct`
--

INSERT INTO `useracct` (`id`, `uname`, `fname`, `mname`, `lname`, `cnum`, `email`, `password`) VALUES
(1, 'harvey', 'harvey', 'a', 'pua', '09350551580', 'puajohnharvey@gmail.com', 'harvey'),
(2, 'hey', 'wapu', 'b', 'wer', '09876543122', 'johnh@gmail.com', '11111'),
(3, 'harveyyyy', 'asas', 'asas', 'pua', '', 'puajozxzxhnharvey@gmail.com', ''),
(4, 'Dynamic', 'Pua', 'p', 'Puaa', '+639350551580', 'abs.@gmail.com', '12345'),
(5, 'mahalkoaics', 'josh', 'b', 'jacinto', '+639154752974', 'joshjacinto@gmail.com', 'kepyas45'),
(6, 'sds', 'harvey', 'ds', 'pua', '+639350551580', 'puaasjohnharvey@gmail.com', '12345');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cancel`
--
ALTER TABLE `cancel`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `image_upload`
--
ALTER TABLE `image_upload`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `login`
--
ALTER TABLE `login`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `payment`
--
ALTER TABLE `payment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `poolreservation`
--
ALTER TABLE `poolreservation`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `resortacct`
--
ALTER TABLE `resortacct`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `resortmng`
--
ALTER TABLE `resortmng`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `status`
--
ALTER TABLE `status`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `trans`
--
ALTER TABLE `trans`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `useracct`
--
ALTER TABLE `useracct`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cancel`
--
ALTER TABLE `cancel`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `login`
--
ALTER TABLE `login`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `poolreservation`
--
ALTER TABLE `poolreservation`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;

--
-- AUTO_INCREMENT for table `resortacct`
--
ALTER TABLE `resortacct`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `resortmng`
--
ALTER TABLE `resortmng`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `status`
--
ALTER TABLE `status`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT for table `trans`
--
ALTER TABLE `trans`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT for table `useracct`
--
ALTER TABLE `useracct`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
