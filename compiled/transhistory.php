
<!DOCTYPE html>
<html lang="en">
<head>

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title>Reservation History</title>
	<link rel="icon" type="text/css" href="image/icon/history.png">

  	<!-- Bootstrap core CSS -->
	<link rel="stylesheet" type="text/css" href="css/bootstrap/bootstrap.min.css">
  	<!-- Custom CSS -->
	<link rel="stylesheet" type="text/css" href="userdes.css">
  	<!-- FONT AWESOME CSS -->
	<link rel="stylesheet" type="text/css" href="fonts/font-awesome.css">
  	<!-- MDB BOOTSTRAP -->
  	<link rel="stylesheet" type="text/css" href="css/mdb/mdb.min.css">


</head>
<body>
    <nav class="user-navbar navbar fixed-top navbar-expand-lg navbar-dark orange ">
      <a class="navbar-brand" href="#">PRIVADO</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarNavDropdown">
        <ul class="navbar-nav w-100">
          <li class="nav-item">
            <a class="nav-link" href="userdash.php">Menu<span class="sr-only">(current)</span></a>
          </li>
          <li class="nav-item dropdown mr-auto">
        <a class="nav-link dropdown-toggle" id="navbarDropdownMenuLink-333" data-toggle="dropdown" aria-haspopup="true"
          aria-expanded="false"><span class="fa fa-user"></span>
        </a>
        <div class="dropdown-menu dropdown-default" aria-labelledby="navbarDropdownMenuLink-333">
            <a class="dropdown-item" href="changepass.php">Change password</a>
          <a class="dropdown-item" href="transhistory.php">History</a>
          <a class="dropdown-item" href="logout.php">log-out</a>
        </div>
      </li>
        </ul>  
      </div>
    </nav><!-- NAVBAR -->

    <br>
    <br>

      <div class="row">
        <div class="col-12">
          <div class="card text-center mt-2">
            <div class="card-header success-color mt-5">
              RESERVATION HISTORY
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-striped table-bordered">
                  <thead>
                    <tr>
                      <th class="text-info">#</th>
                      <th class="text-warning">Name of resort</th>
                      <th class="text-warning">Hour of stay</th>
                      <th class="text-warning">Add-on</th>
                      <th class="text-warning">Date</th>
                    </tr>
                   </thead>
                   <tbody>
                 <?php  
				session_start();  
				if(!isset($_SESSION["uname"]))
				{
				 header("location:userdash.php");

				}else{
					  include('db.php');
                      $tsql = "select * from trans where uname='".$_SESSION['uname']."'";
                      $tre = mysqli_query($con,$tsql);
                      while($trow=mysqli_fetch_array($tre) )
                      { 
                   
                      echo"<tr>
                      <th>".$trow['id']."</th>
                      <th>".$trow['resortname']."</th>
                      <th>".$trow['hourstay']."</th>
                      <th>".$trow['add on']."</th>
                      <th>".$trow['cin']."</th>
                      </tr>";
                      } 
                                  
                      }
                      ?>
                   </tbody>
                </table>
              </div>
            </div>
          </div>
          <div class="card text-center mt-2">
            <div class="card-header success-color mt-5">
              STAUS OF RESERVATION
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-striped table-bordered">
                  <thead>
                    <tr>
                      <th class="text-info">#</th>
                      <th class="text-warning">Name of resort</th>
                      <th class="text-warning">Hour of stay</th>
                      <th class="text-warning">Add-on</th>
                      <th class="text-warning">Date</th>
                      <th class="text-warning">Status</th>
                    </tr>
                   </thead>
                   <tbody>
                 <?php  
			
					  include('db.php');
                      $tsql = "select * from status where uname='".$_SESSION['uname']."'";
                      $tre = mysqli_query($con,$tsql);
                      while($trow=mysqli_fetch_array($tre) )
                      { 
                   
                      echo"<tr>
                      <th>".$trow['id']."</th>
                      <th>".$trow['resortname']."</th>
                      <th>".$trow['hourstay']."</th>
                      <th>".$trow['add on']."</th>
                      <th>".$trow['cin']."</th>
                      <th>".$trow['stat']."</th>
                      </tr>";
                      } 
                                  
                      
                      ?>
                   </tbody>
                </table>
              </div>
            </div>
          </div>
          <div class="card text-center mt-2">
            <div class="card-header success-color mt-5">
              PAYMENT HISTORY
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-striped table-bordered">
                  <thead>
                    <tr>
                      <th class="text-info">#</th>
                      <th class="text-warning">Name of resort</th>
                      <th class="text-warning">Date</th>
                      <th class="text-warning">Hour of stay</th>
                      <th class="text-warning">Add-on</th>
                      <th class="text-warning">Deposit</th>
                      <th class="text-warning">Total Payment</th>
                      <th class="text-warning">Balance</th>
                      <th class="text-warning">Status</th>
                      <th class="text-warning">Receipt</th>
                    </tr>
                   </thead>
                   <tbody>
                 <?php  
                 include('db.php');
                      $unames = $_COOKIE['uname'];
                      $query = "select * from payment where uname='".$_COOKIE['uname']."'";
                      $tree = mysqli_query($con,$query);
                      while($trow=mysqli_fetch_array($tree))
                      { 
                   
                      echo"<tr>
                      <th>".$trow['id']."</th>
                      <th>".$trow['resortname']."</th>
                      <th>".$trow['cin']."</th>
                      <th>".$trow['hourstay']."</th>
                      <th>".$trow['add on']."</th>
                      <th>".$trow['deposit']."</th>
                      <th>".$trow['ftotpay']."</th>
                      <th>".$trow['balance']."</th>
                      <th>".$trow['stat']."</th>
                      <th><a href=print.php?pid=".$trow['id']." <button class='btn btn-primary'> <i class='fa fa-print' ></i> Print</button></th>
                      </tr>";
                      } 
                                  
                      ?>
                   </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>


<!-- FOOTER -->
    <div class="footer">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="footer-copyright text-center mt-2 py-3">© 2019 Copyright:
   	 					<a href="website.html"> Brgy Pansol Calamba City, Laguna</a>
			  		</div>
                </div>
            </div>
        </div>
    </div>
<!-- FOOTER -->


	  <!-- jQuery-2.2.4 js -->
    <script src="js/jquery/jquery-2.2.4.min.js"></script>
    <!-- Popper js -->
    <script src="js/bootstrap/popper.min.js"></script>
    <!-- Bootstrap-4 js -->
    <script src="js/bootstrap/bootstrap.min.js"></script>
    <!-- All Plugins js -->
    <script src="js/others/plugins.js"></script>
    <!-- Active JS -->
    <script src="js/active.js"></script>

                      


</script>
</body>
</html>