<?php
include('db.php');
?>
<?php  
session_start();  
if(!isset($_SESSION["uname"]))
{
 header("location:index.php");
}else{

	  if(isset($_POST['submit2'])){
	  include('db.php');
	  $date=$_POST['cin'];
	  $day=$_POST['day'];
	  $hour=$_POST['hourstay'];
	  $res=$_POST['resort'];
	  $thrs=$_POST['12h'];
	  $tfhrs=$_POST['24h'];
	  $sql = "SELECT cin, hourstay, mor FROM poolreservation WHERE cin = '$date' AND resortname = '$res' AND hourstay = '$hour'";
      $result = mysqli_query($con,$sql);

      if(mysqli_num_rows($result)>0)
	  { 
	  	while($row=mysqli_fetch_array($result))
			{
				$stay = $row['hourstay'];
				$dy = $row['mor'];

				if ($stay == "24 hrs") {
					echo "<script type='text/javascript'> alert('This date is already reserved')</script>";
				}
				else{
					if ($dy == $day) {
						echo "<script type='text/javascript'> alert('The selected session is not available')</script>";
					}
					else{
						setcookie('dates',$date);
						setcookie('day',$day);
						setcookie('hours',$hour);
						setcookie('res',$res);
						setcookie('h12',$thrs);
						setcookie('h24',$tfhrs);
						echo "<script type='text/javascript'> window.location='reservation.php'</script>";
						
					}
				}
			}
	 }
	  else
	 {
	 	setcookie('dates',$date);
		setcookie('day',$day);
		setcookie('hours',$hour);
		setcookie('res',$res);
		setcookie('h12',$thrs);
		setcookie('h24',$tfhrs);
	 	echo "<script type='text/javascript'> window.location='reservation.php'</script>";
	 }
	}
}
?>
<?php
if(isset($_POST['value']))
{
$resort=$_POST['resort'];
$thrs=$_POST['12hrs'];
$tfhrs=$_POST['24hrs'];
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
    <meta name="description" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<title>Reservation Form</title>
	<link rel="icon" href="image/icon/book.png">


	<!-- Bootstrap CSS -->
    <link href="css/bootstrap/bootstrap.min.css" type="text/css" rel="stylesheet">
    <!-- MDB BOOTSTRAP -->
    <link rel="stylesheet" type="text/css" href="css/mdb/mdb.min.css">
    <!-- Jquery CSS -->
    <link rel="stylesheet" type="text/css" href="js/jquery/jquery-ui.css">


</head>
<body>
	<!--Navbar -->
<nav class="mb-1 navbar navbar-expand-lg amy-crisp-gradient lighten-1">
  <a class="navbar-brand white-text" href="#">PRIVADO</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent-555"
    aria-controls="navbarSupportedContent-555" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarSupportedContent-555">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item ">
        <a class="nav-link black-text" href="userdash.php">Menu
          <span class="sr-only">(current)</span>
        </a>
      </li>
      <li class="nav-item dropdown mr-auto">
        <a class="nav-link dropdown-toggle" id="navbarDropdownMenuLink-333" data-toggle="dropdown" aria-haspopup="true"
          aria-expanded="false">
        </a>
        <div class="dropdown-menu dropdown-default" aria-labelledby="navbarDropdownMenuLink-333">
          <a class="dropdown-item" href="changepass.php">Change password</a>
          <a class="dropdown-item" href="transhistory.php">History</a>
          <a class="dropdown-item" href="logout.php">log-out</a>
        </div>
      </li>
    </ul>
  </div>
</nav>
<!-- Navbar -->


<div class="row">
	<div class="col-12">
		<form method="post">
		<div class="card mx-xl-5 mt-2">
		    <div class="card-body">
		        <p class="h4 text-center py-4 gradient-card-header young-passion-gradient white-text">AVAILABILITY CHECK</p>
		        	<div>
		            	<label>Selected resort: <?php echo $resort?></label>
		            	<input type="hidden" name="resort" value="<?php echo $resort?>">
					</div>
		        	<div>
			            <label>Select Hours</label>
			            <input type="radio" id="hourstay" name="hourstay" value="12 hrs"> 12 hrs
			            <input type="radio" id="hour" name="hourstay" value="24 hrs"> 24 hrs
			            	<div style="display:none" id="displayhour">
	                        	<div class="checkbox-list">
	                          		<label class=" ml-4">
	                            		<input type="radio" id="day" name="day" value="day"> Day
	                            		<input type="radio" id="day" name="day" value="night"> Night
	                         		 </label>
	                        	</div>
                    		</div>
                    		<div style="display:none" id="displaynight">
	                        	<div class="checkbox-list">
	                          		<label class=" ml-4">
	                            		<input type="radio" id="day" name="day" value="day"> Day
	                            		<input type="radio" id="day" name="day" value="night"> Night
	                         		 </label>
	                        	</div>
                    		</div>
		        	</div>
		            <br>
		            <div>
		            	<label>Selected resort: <?php echo $thrs?></label>
		            	<input type="hidden" name="12h" value="<?php echo $thrs?>">
					</div>
					<br>
					<div>
						<label>Selected resort: <?php echo $tfhrs?></label>
		            	<input type="hidden" name="24h" value="<?php echo $tfhrs?>">
					</div>
					<br>
					<div class="form-group">
						<label>Select Date</label>
    					<input type="text" name="cin" id="datetimepicker" class="form-control" required autocomplete="off">
					</div>
					<input type="submit" name="submit2" class="btn night-fade-gradient" value="Check Availability">
			</div><!-- card-body -->
		</div><!-- card -->
		
</form>
  	</div>          
</div><!-- row --> 






    <!-- jQuery-2.2.4 js -->
    <script src="js/jquery/jquery-2.2.4.min.js"></script>
    <!-- Popper js -->
    <script src="js/bootstrap/popper.min.js"></script>
    <!-- Bootstrap-4 js -->
    <script src="js/bootstrap/bootstrap.min.js"></script>
    <!-- Bootstrap-4 js -->
    <script src="js/bootstrap/bootstrap.min.js"></script>

    <!-- SCRIPTS -->

    <!-- JQuery -->
    <script type="text/javascript" src="js/mdb/jquery-3.3.1.min.js"></script>
    <!-- Bootstrap tooltips -->
    <script type="text/javascript" src="js/mdb/popper.min.js"></script>
    <!-- MDB core JavaScript -->
    <script type="text/javascript" src="js/mdb/mdb.js"></script>
    <!-- Bootstrap core JavaScript -->
    <script src="js/adminjs/jquery.min.js"></script>
    <!-- Bootstrap Javascript -->
    <script src="js/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- JQuery -->
    <script type="text/javascript" src="js/jquery/jquery-3.3.1.js"></script>
    <!-- JQuery -->
    <script type="text/javascript" src="js/jquery/jquery-ui.js"></script>


    <!-- Date time picker -->
    <script type="text/javascript">
		$(function(){
			$("#datetimepicker").datepicker({
				dateFormat :"yy/mm/dd",
				minDate: 0
			});
		});
    </script>
    
    <!-- HOURSTAY -->
    <script>
	$("input[type='radio'][id='hourstay']").on("change",function(){
      if($(this).is(':checked')){
        $("#displayhour").show();
      }
      else{
        $("#displayDS").hide();
      }
    });
    </script>
    <script>
	$("input[type='radio'][id='hour']").on("change",function(){
      if($(this).is(':checked')){
        $("#displayhour").hide();
      }
    });
    </script>

    <script>
	$("input[type='radio'][id='hour']").on("change",function(){
      if($(this).is(':checked')){
        $("#displaynight").show();
      }
      else{
        $("#displaynight").hide();
      }
    });
    </script>
    <script>
	$("input[type='radio'][id='hourstay']").on("change",function(){
      if($(this).is(':checked')){
        $("#displaynight").hide();
      }
    });
    </script>
</body>
</html>

	