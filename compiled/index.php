
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
    <meta name="description" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">


   
	<title>Private Pool Reservation</title>
	 <link rel="icon" href="image/palmtree.png">
    
    
    <!--CORE STYLESHEET-->
    <link rel="stylesheet"  type="text/css" href="style.css">
	<!-- Bootstrap CSS -->
    <link href="css/bootstrap/bootstrap.min.css" rel="stylesheet">
    <!-- MDB BOOTSTRAP -->
    <link rel="stylesheet" type="text/css" href="css/mdb/mdb.min.css">
    <!-- Additional CSS-->
    <link href="css/responsive/responsive.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="css/others/animate.css">
    <!-- Additional CSS-->
    <link href="css/others/font-awesome.min.css" rel="stylesheet">

    
</head>

    <!--  Header Area Start  -->
<body>
    <header class="header_area" id="header">
        <div class="container-fluid h-100">
            <div class="row h-100">
                <div class="col-12 h-100">
                    <nav class="h-100 navbar navbar-expand-md ">
                        <a class="navbar-brand" href="index.php"><img class="image" src="image/palmtree.png" alt="LOGO"> Pansol</a>
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#mynav" aria-controls="mynav" aria-expanded="false" aria-label="Toggle navigation"><span class="fa fa-bars"></span></button>
                        <!-- Nav -->
                        <div class="collapse navbar-collapse" id="mynav">
                            <ul class="navbar-nav mr-auto">
                                <li class="nav-item active">
                                    <a class="nav-link green-text" href="index.php">Home <span class="sr-only">(current)</span></a>
                                </li>
                                <li class="nav-item active">
                                    <a class="nav-link green-text" href="signup.php">Sign up</a>
                                </li>
                            </ul>
                            <div class="ml-2">
                                <button class="log-in-btn text-white" data-toggle='modal' data-target="#modalLoginForm">Log-in</button>
                            </div>
                            <!-- <div class="ml-2">
                                <button class="log-in-btn text-white" data-toggle='modal' data-target="#LoginForm">Dynamic Log-in</button>
                            </div> -->
                        </div>
                    </nav>
                </div>
            </div>
        </div>
    </header>
    <!--  Header Area End  -->

    <!--  Welcome Area Start  -->
    <section class="welcome-area bg-img bg-overlay" style="background-image: url(image/resort/Valerie/valerie18.jpg);">
        <div class="container h-100">
            <div class="row h-100 align-items-center justify-content-center">
                <div class="col-12 col-md-10">
                    <div class="content">
                        <h2>Take a vacation near you</h2>
                        <h4>This is your guide like no other.</h4>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- ***** Welcome Area End ***** -->

    <!-- ***** Catagory Area Start ***** -->
    <section class="catagory-area">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="all-catagories">
                        <div class="row">
                            <!-- Single Catagory Area -->
                            <div class="col-12 col-xs-6 col-md">
                                <div class="single-catagory-area wow fadeInUpBig" data-wow-delay="0.2s">
                                    <div class="catagory-content">
                                        <img src="image/icon/icon-1.png" alt="pools">
                                        <h6>POOLS</h6>
                                    </div>
                                </div>
                            </div>
                            <!-- Single Catagory Area -->
                            <div class="col-12 col-xs-6 col-md">
                                <div class="single-catagory-area wow fadeInUpBig" data-wow-delay="0.4s">
                                    <div class="catagory-content">
                                        <img src="image/icon/icon-2.png" alt="Convience">
                                        <h6>Convience</h6>
                                    </div>
                                </div>
                            </div>
                            <!-- Single Catagory Area -->
                            <div class="col-12 col-xs-6 col-md">
                                <div class="single-catagory-area wow fadeInUpBig" data-wow-delay="0.6s">
                                    <div class="catagory-content">
                                        <img src="image/icon/icon-3.png" alt="Privacy">
                                        <h6>Privacy</h6>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--  Catagory Area End -->

    <!--  About Area Start  -->
    <section class="about-area section-padding-0-100">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="about-content text-center">
                        <h2>Reserve your pool<br><span>with few clicks</span></h2>
                        <p>This is the first ever of its kind online private pool reservation dedicated for everyone else who wants to take a vaction, get out of the city life and just relax.</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--  About Area End  -->



<!-- Modal -->

<div class="modal fade" id="modalLoginForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
  aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header text-center">
        <h4 class="modal-title w-100 font-weight-bold">Log-in</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body mx-3">
        <form action="login.php" method="post">
        <div class="md-form mb-5">
          <!-- <i class="fas fa-envelope prefix grey-text"></i> -->
          <input type="text" id="user" name="user" class="form-control">
          <label  for="defaultForm-email">Your username</label>
        </div>

        <div class="md-form mb-4">
          <!-- <i class="fas fa-lock prefix grey-text"></i> -->
          <input type="password" id="pass" name="pass" class="form-control">
          <label  for="defaultForm-pass">Your password</label>
        </div>

      </div>
      <div class="modal-footer d-flex justify-content-center">
        <button class="btn btn-default">Login</button>
      </div>
     </form>
    </div>
  </div>
</div>


<!-- Modal -->

<!-- Dynamic Modal -->

<!-- <div class="modal fade" id="LoginForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
  aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header text-center">
        <h4 class="modal-title w-100 font-weight-bold">Log-in</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body mx-3">
        <form action="dynamiclogin.php" method="post">
        <div class="md-form mb-5">
          <i class="fas fa-envelope prefix grey-text"></i>
          <input type="text" id="user" name="user" class="form-control">
          <label  for="defaultForm-email">Your username</label>
        </div>

        <div class="md-form mb-4">
          <i class="fas fa-lock prefix grey-text"></i>
          <input type="password" id="pass" name="pass" class="form-control">
          <label  for="defaultForm-pass">Your password</label>
        </div>

      </div>
      <div class="modal-footer d-flex justify-content-center">
        <button class="btn btn-default">Login</button>
      </div>
     </form>
    </div>
  </div>
</div> -->


<!-- Dynamic Modal -->


<!-- Footer  Start -->
<footer class="page-footer font-small blue">


<div class="footer-copyright text-center mt-2 py-3">© 2019 Copyright:
    <a href="website.html"> Brgy Pansol Calamba City, Laguna</a>
</div>


</footer>
<!--  Footer End  -->


    <!-- JS Scripts-->

    <!-- jQuery-2.2.4 js -->
    <script src="js/jquery/jquery-2.2.4.min.js"></script>
    <!-- Popper js -->
    <script src="js/bootstrap/popper.min.js"></script>
    <!-- Bootstrap-4 js -->
    <script src="js/bootstrap/bootstrap.min.js"></script>
    <!-- All Plugins js -->
    <script src="js/others/plugins.js"></script>
    <!-- Active JS -->
    <script src="js/active.js"></script>


    <!-- JQuery -->
    <script type="text/javascript" src="js/mdb/jquery-3.3.1.min.js"></script>
    <!-- Bootstrap tooltips -->
    <script type="text/javascript" src="js/mdb/popper.min.js"></script>
    <!-- MDB core JavaScript -->
    <script type="text/javascript" src="js/mdb/mdb.js"></script>
    <!-- Bootstrap core JavaScript -->
    <script src="js/adminjs/jquery.min.js"></script>


</body>
</html>