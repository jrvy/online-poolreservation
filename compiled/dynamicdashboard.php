<?php  
session_start();  
if(!isset($_SESSION["uname"]))
{
 header("location:website.php");
}
?> 
<!DOCTYPE html>
<html lang="en">
<head>

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title>Private Pool Reservation</title>
	<link rel="icon" type="text/css" href="image/palmtree.png">

  <!-- Bootstrap core CSS -->
	<link rel="stylesheet" type="text/css" href="css/bootstrap/bootstrap.min.css">
  <!-- Custom CSS -->
	<link rel="stylesheet" type="text/css" href="userdes.css">
  <!-- FONT AWESOME CSS -->
	<link rel="stylesheet" type="text/css" href="fonts/font-awesome.css">
  <!-- MDB BOOTSTRAP -->
  <link rel="stylesheet" type="text/css" href="css/mdb/mdb.min.css">


</head>
<body>
    <nav class="user-navbar navbar fixed-top navbar-expand-lg navbar-dark orange ">
      <a class="navbar-brand" href="#">PRIVADO</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarNavDropdown">
        <ul class="navbar-nav w-100">
          <li class="nav-item active">
            <a class="nav-link active" href="userdash.html">Menu<span class="sr-only">(current)</span></a>
          </li>
          <li class="nav-item dropdown mr-auto">
        <a class="nav-link dropdown-toggle" id="navbarDropdownMenuLink-333" data-toggle="dropdown" aria-haspopup="true"
          aria-expanded="false"><?php echo $_SESSION['uname'] ?>
        </a>
        <div class="dropdown-menu dropdown-default" aria-labelledby="navbarDropdownMenuLink-333">
          <a class="dropdown-item" href="transhistory.php">History</a>
          <a class="dropdown-item" href="logout.php">log-out</a>
        </div>
      </li>
        </ul>  
      </div>
    </nav><!-- NAVBAR -->

<!-- CAROUSEL -->
    <div class="slider">
        <div class="container">
            <div id="carouselIndicators" class="carousel slide" data-ride="carousel">
              <ol class="carousel-indicators">
                <li data-target="#carouselIndicators" data-slide-to="0" class="active"></li>
                <li data-target="#carouselIndicators" data-slide-to="1"></li>
                <li data-target="#carouselIndicators" data-slide-to="2"></li>
                <li data-target="#carouselIndicators" data-slide-to="3"></li>
                <li data-target="#carouselIndicators" data-slide-to="4"></li>
                <li data-target="#carouselIndicators" data-slide-to="5"></li>
                <li data-target="#carouselIndicators" data-slide-to="6"></li>
                <li data-target="#carouselIndicators" data-slide-to="7"></li>
                <li data-target="#carouselIndicators" data-slide-to="8"></li>
                <li data-target="#carouselIndicators" data-slide-to="9"></li>
                <li data-target="#carouselIndicators" data-slide-to="10"></li>
                <li data-target="#carouselIndicators" data-slide-to="11"></li>
                <li data-target="#carouselIndicators" data-slide-to="12"></li>
                <li data-target="#carouselIndicators" data-slide-to="13"></li>
                <li data-target="#carouselIndicators" data-slide-to="14"></li>
              </ol>
              <div class="carousel-inner">
                <div class="carousel-item active">
                  <img src="image/resort/Valerie/valerie18.jpg" class="d-block w-100" alt="RESORT">
                    <div class="carousel-caption d-none d-md-block">
                      <h5>Valerie Private Pool</h5>
                    </div> 
                </div>
                <div class="carousel-item">
                  <img src="image/resort/carlos/carlos3.jpg" class="d-block w-100" alt="RESORT">
                    <div class="carousel-caption d-none d-md-block">
                      <h5>Carlos Private Pool</h5>
                    </div> 
                </div>
                <div class="carousel-item">
                  <img src="image/resort/elw/elw7.jpg" class="d-block w-100" alt="RESORT">
                    <div class="carousel-caption d-none d-md-block">
                      <h5>ELW Private Pool</h5>
                    </div> 
                </div>
                <div class="carousel-item">
                  <img src="image/resort/lagunablue/lagunablue2.jpg" class="d-block w-100" alt="RESORT">
                    <div class="carousel-caption d-none d-md-block">
                      <h5>Laguna Blue Private Pool</h5>
                    </div> 
                </div>
                <div class="carousel-item">
                  <img src="image/resort/lenzen/lenzen2.jpg" class="d-block w-100" alt="RESORT">
                    <div class="carousel-caption d-none d-md-block">
                      <h5>Lenzen Azy Private Pool</h5>
                    </div> 
                </div>
                <div class="carousel-item">
                  <img src="image/resort/rubi/rubi3.jpg" class="d-block w-100" alt="RESORT">
                    <div class="carousel-caption d-none d-md-block">
                      <h5>Rubi Rosa Private Pool</h5>
                    </div> 
                </div>
                <div class="carousel-item">
                  <img src="image/resort/vergara/vergara9.jpg" class="d-block w-100" alt="RESORT">
                    <div class="carousel-caption d-none d-md-block">
                      <h5>Vergara Private Pool</h5>
                    </div> 
                </div>
                <div class="carousel-item">
                  <img src="image/resort/Floredeliza/Floredeliza4.jpg" class="d-block w-100" alt="RESORT">
                    <div class="carousel-caption d-none d-md-block">
                      <h5>Floredeliza Private Pool</h5>
                    </div> 
                </div>
                <div class="carousel-item">
                  <img src="image/resort/Floredeliza/Floredeliza9.jpg" class="d-block w-100" alt="RESORT">
                    <div class="carousel-caption d-none d-md-block">
                      <h5>Floredeliza Private Pool</h5>
                    </div> 
                </div>
                <div class="carousel-item">
                  <img src="image/resort/INLG/INLG14.jpg" class="d-block w-100" alt="RESORT">
                    <div class="carousel-caption d-none d-md-block">
                      <h5>INLG Private Pool</h5>
                    </div> 
                </div>
                <div class="carousel-item">
                  <img src="image/resort/jocris3/jocris32.jpg" class="d-block w-100" alt="RESORT">
                    <div class="carousel-caption d-none d-md-block">
                      <h5>Villa Jocris 3 Private Pool</h5>
                    </div> 
                </div>
                <div class="carousel-item">
                  <img src="image/resort/Valerie/valerie19.jpg" class="d-block w-100" alt="RESORT">
                    <div class="carousel-caption d-none d-md-block">
                      <h5>Valerie Private Pool</h5>
                    </div> 
                </div>
                <div class="carousel-item">
                  <img src="image/resort/Villa Leonora/leonora18.jpg" class="d-block w-100" alt="RESORT">
                    <div class="carousel-caption d-none d-md-block">
                      <h5>Villa Leonora Private Pool</h5>
                    </div> 
                </div>
                <div class="carousel-item">
                  <img src="image/resort/Villa Leonora/leonora10.jpg" class="d-block w-100" alt="RESORT">
                    <div class="carousel-caption d-none d-md-block">
                      <h5>Villa Leonora Private Pool</h5>
                    </div> 
                </div>
                <div class="carousel-item">
                  <img src="image/resort/Villa Leonora/leonora9.jpg" class="d-block w-100" alt="RESORT">
                    <div class="carousel-caption d-none d-md-block">
                      <h5>Villa Leonora Private Pool</h5>
                    </div> 
                </div>
              </div>
              <a class="carousel-control-prev" href="#carouselIndicators" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
              </a>
              <a class="carousel-control-next" href="#carouselIndicators" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
              </a>
        </div>
    </div>
  </div>
<!-- CAROUSEL -->


<!-- MAIN CONTENT -->

<div class="main-content">
    <div class="container">
        <h5>Resorts</h5>
            <div class="row">
   <?php
            include ('db.php');
            $sql = "select * from resortmng";
            $re = mysqli_query($con,$sql);
            while($row= mysqli_fetch_array($re))
                    {
                        $id = $row['type'];
                      if($id == "Family resort") 
                {          
             echo"<div class='col-md-6'>
                <div class='card card-cascade wider'> 
                  <div class='view view-cascade overlay'>
                  <img class='card-img-top' src='image/resort/alegreya/alegreya11.jpg' alt='Resort'>
                  </div>
                    <!-- Card content -->
                    <div class='card-body card-body-cascade'>
                    <!-- Title -->
                    <h6 class='card-title text-center'><strong>".$row['rname']."</strong></h6>
                    <!-- Subtitle -->
                    <h6 class='card-text red-text pt-3'><strong><h5>Location: </h5> ".$row['loc']."</strong></h6>
                    <!-- Text -->   
                    <p class='card-text'>A family type private resort on economic price.</p>
                    <p class='card-text'><strong>Rent:</strong> ".$row['r12']." Php-".$row['r24']." Php"."</p>
                    <form action=\"dynamicresort.php\" method=\"post\">
                    <input type=\"hidden\" name=\"resort\" value='".$row['rname']."'>
                    <button class='btn sunny-morning-gradient'<span class='fa fa-eye'></span> View more</button>
                  </div>
                  </form>
                </div> 
              </div>";
              }
                else if ($id == "Debut, wedding type resort")
                      
              {
              echo"<div class='col-md-6'>
                <div class='card card-cascade wider'> 
                  <div class='view view-cascade overlay'>
                    <img class='card-img-top' src='image/resort/carlos/carlos3.jpg' alt='Resort'>
                      
                  </div>
                  <!-- Card content -->
                    <div class='card-body card-body-cascade'>
                  <!-- Title -->
                    <h6 class='card-title text-center'><strong>".$row['rname']."</strong></h6>
                  <!-- Subtitle -->
                    <h6 class='card-text red-text pt-3'><strong><h5>Location: </h5>".$row['loc']."</strong></h6>
                  <!-- Text -->
                    <p class='card-text'>Large resort with giant slide, karaoke and billiards.</p>
                    <p class='card-text'><strong>Rent:</strong>".$row['r12']."-".$row['r24']."</p>
                    <input type='hidden' name='resort' value='".$row['rname']."'>
                    <button class='btn sunny-morning-gradient' href='dynamicresort.php' role='button' name='value'><span class='fa fa-eye'></span> View more</button>
                  </div>
                </div> 
              </div>";
            }
              }
?>
            </div> <!-- row -->

    </div> <!-- container -->
</div> <!-- Main Content -->

<!-- FOOTER -->
    <div class="footer">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <p>Copyright PUA,TILA,JAVIER,LUMBRES 2019</p>
                </div>
            </div>
        </div>
    </div>
<!-- FOOTER -->


	  <!-- jQuery-2.2.4 js -->
    <script src="js/jquery/jquery-2.2.4.min.js"></script>
    <!-- Popper js -->
    <script src="js/bootstrap/popper.min.js"></script>
    <!-- Bootstrap-4 js -->
    <script src="js/bootstrap/bootstrap.min.js"></script>
    <!-- All Plugins js -->
    <script src="js/others/plugins.js"></script>
    <!-- Active JS -->
    <script src="js/active.js"></script>

                      


</script>
</body>
</html>