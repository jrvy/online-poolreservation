
<!DOCTYPE html>
<html lang="en">
<head>

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title>Reservation History</title>
	<link rel="icon" type="text/css" href="image/icon/history.png">

  	<!-- Bootstrap core CSS -->
	<link rel="stylesheet" type="text/css" href="css/bootstrap/bootstrap.min.css">
  	<!-- Custom CSS -->
	<link rel="stylesheet" type="text/css" href="userdes.css">
  	<!-- FONT AWESOME CSS -->
	<link rel="stylesheet" type="text/css" href="fonts/font-awesome.css">
  	<!-- MDB BOOTSTRAP -->
  	<link rel="stylesheet" type="text/css" href="css/mdb/mdb.min.css">


</head>
<body>
    <nav class="user-navbar navbar fixed-top navbar-expand-lg navbar-dark orange ">
      <a class="navbar-brand" href="#">PRIVADO</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarNavDropdown">
        <ul class="navbar-nav w-100">
          <li class="nav-item">
            <a class="nav-link" href="userdash.php">Menu<span class="sr-only">(current)</span></a>
          </li>
          <li class="nav-item dropdown mr-auto">
        <a class="nav-link dropdown-toggle" id="navbarDropdownMenuLink-333" data-toggle="dropdown" aria-haspopup="true"
          aria-expanded="false"><span class="fa fa-user"></span>
        </a>
        <div class="dropdown-menu dropdown-default" aria-labelledby="navbarDropdownMenuLink-333">
          <a class="dropdown-item" href="transhistory.php">History</a>
          <a class="dropdown-item" href="logout.php">log-out</a>
        </div>
      </li>
        </ul>  
      </div>
    </nav><!-- NAVBAR -->

    <br>
    <br>

      <div class="row">
        <div class="col-12">
          <form method="post">
            <div class="card mx-xl-5 mt-2 ">
              <div class="card-body ">
                <legend class="gradient-card-header young-passion-gradient">UPDATE PASSWORD</legend>
                <div class="md-form">
                    <label  class="grey-text font-weight-light">New password</label>
                    <input type="password" id="pass" name="pass" class="form-control" required autocomplete="off">
                </div>
                <div class="md-form">
                    <label  class="grey-text font-weight-light">Confirm password</label>
                    <input type="password" id="cpass" name="cpass" class="form-control" required autocomplete="off">
                </div>
                <input type="submit" name="submit1" class="btn night-fade-gradient">
                <?php  
                session_start();  
                if(!isset($_SESSION["uname"]))
                {
                 header("location:index.php");

                }else{
                  if(isset($_POST['submit1']))
                  { 
                    include('db.php');
                      $update = "UPDATE `useracct` SET `password` ='".$_POST['cpass']."' WHERE uname='".$_SESSION['uname']."'";
                      
                        if(mysqli_query($con,$update))
                          {
                            echo "<script  type='text/javascript'> alert('Password updated') </script>";
                            echo "<script type='text/javascript'> window.location='userdash.php'</script>";
                          }
                        else{
                            echo "<script  type='text/javascript'> alert('Update failed') </script>";
                        }
                  }        
                }
                  ?>
                </div>
              </div>
            </form>
        </div>
      </div>



	  <!-- Js Validate -->
    <script src="bootstrap-validate-master/dist/bootstrap-validate.js"></script>
    <!-- jQuery-2.2.4 js -->
    <script src="js/jquery/jquery-2.2.4.min.js"></script>
    <!-- Popper js -->
    <script src="js/bootstrap/popper.min.js"></script>
    <!-- Bootstrap-4 js -->
    <script src="js/bootstrap/bootstrap.min.js"></script>
    <!-- Bootstrap-4 js -->
    <script src="js/bootstrap/bootstrap.min.js"></script>

    <!-- SCRIPTS -->

    <!-- JQuery -->
    <script type="text/javascript" src="js/mdb/jquery-3.3.1.min.js"></script>
    <!-- Bootstrap tooltips -->
    <script type="text/javascript" src="js/mdb/popper.min.js"></script>
    <!-- MDB core JavaScript -->
    <script type="text/javascript" src="js/mdb/mdb.js"></script>
    <!-- Bootstrap core JavaScript -->
    <script src="js/adminjs/jquery.min.js"></script>
    <!-- Bootstrap Javascript -->
    <script src="js/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script>
    bootstrapValidate('#pass', 'min:8:Minimum of 8 characters');
    bootstrapValidate('#cpass', 'matches:#pass:Your password does not match');
    </script>                  


</script>
</body>
</html>