<!-- <?php  
session_start();  
if(!isset($_SESSION["uname"]))
{
 header("location:website.php");
}else{
  if(isset($_POST['resort']))
    {
    include('db.php');
    $resort=$_POST['resort'];
      $sql ="SELECT * FROM resortmng WHERE rname='".$_POST['resort']."'";
        $re = mysqli_query($con,$sql);
        while($row=mysqli_fetch_array($re))
        {
          $rname=$row['rname'];
          $loc=$row['loc'];
          $type=$row['type'];
          $cnum=$row['cnum'];
          $r12=$row['r12'];
          $r24=$row['r24'];
          $ppool=$row['ppool'];
          $proom=$row['proom'];
          $pgen=$row['pgen'];
          $rmord=$row['rmord'];
          $rmair=$row['rmair'];
          $pldepthkiddie=$row['pldepthkiddie'];
          $pldepthadult=$row['pldepthadult'];
          $plkiddie=$row['plkiddie'];
          $pladult=$row['pladult'];
          $inckaraoke=$row['inckaraoke'];
          $incbilliards=$row['incbilliards'];
          $gasul=$row['gasul'];
          $maxper=$row['maxper'];
        }

    }
}
?> -->
<!DOCTYPE html>
<html lang="en">
<head>
	  <meta charset="utf-8">
    <meta name="description" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	  <title><?php echo $rname?></title>
    <link rel="icon" href="image/palmtree.png">

	  <!-- Bootstrap CSS -->
    <link href="css/bootstrap/bootstrap.min.css" type="text/css" rel="stylesheet">
    <!-- MDB BOOTSTRAP -->
    <link rel="stylesheet" type="text/css" href="css/mdb/mdb.min.css">
    <!-- Custom CSS -->
    <link rel="stylesheet"  href="resortstyle.css">
    
</head>


<body>
<!--Navbar -->
<nav class="mb-1 navbar navbar-expand-lg navbar-dark orange lighten-1">
  <a class="navbar-brand" href="#">PRIVADO</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent-555"
    aria-controls="navbarSupportedContent-555" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarSupportedContent-555">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item ">
        <a class="nav-link" href="userdash.html">Menu
          <span class="sr-only">(current)</span>
        </a>
      </li>
      <li class="nav-item dropdown mr-auto">
        <a class="nav-link dropdown-toggle" id="navbarDropdownMenuLink-333" data-toggle="dropdown" aria-haspopup="true"
          aria-expanded="false"><?php echo $_SESSION['uname'] ?>
        </a>
        <div class="dropdown-menu dropdown-default" aria-labelledby="navbarDropdownMenuLink-333">
          <a class="dropdown-item" href="../transhistory.php">History</a>
          <a class="dropdown-item" href="logout.php">log-out</a>
        </div>
      </li>
    </ul>
  </div>
</nav>
<!-- Navbar -->


<!-- CAROUSEL -->
<div class="container">
<div id="carousel" class="carousel slide mt-4 " data-ride="carousel">
  <ol class="carousel-indicators">
    <li data-target="#carousel" data-slide-to="0" class="active"></li>
    <li data-target="#carousel" data-slide-to="1"></li>
    <li data-target="#carousel" data-slide-to="2"></li>
  </ol>
  <div class="carousel-inner">
    <div class="carousel-item active">
      <img src="image/resort/alegreya/alegreya.jpg" class="d-block w-100" alt="Alegria resort">
          <div class="carousel-caption d-none d-md-block">
             <h5><?php echo $rname?></h5>
             <p><?php echo $loc?></p>
          </div>   
    </div>
</div>
</div>
<!-- END CAROUSEL -->

<!-- JUMBOTRON -->
<div class="jumbotron mt-4 mb-4 mx-2">
  <h1 class="display-4 text-success"><?php echo $rname?></h1>

  <p class="lead text-warning">A simple and economical price for a private resort located at Brgy Pansol Calamba City,Laguna.The price is budget friendly good for family outing, company outing or just want escape from the stress of life.</p>
  <hr class="my-4">
  <!-- Tabs -->
  <div class="row">
    <div class="col-sm-12 col-12">
      <ul class="nav nav-tabs" id="myTab" role="tablist">
        <li class="nav-item">
          <a class="nav-link active" id="information-tab" data-toggle="tab" href="#information" role="tab" aria-controls="information"
            aria-selected="true">Information</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" id="pool-tab" data-toggle="tab" href="#pool" role="tab" aria-controls="pool"
            aria-selected="false">Pools</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" id="room-tab" data-toggle="tab" href="#room" role="tab" aria-controls="room"
            aria-selected="false">Rooms</a>
        </li>
      </ul>
        <div class="tab-content" id="myTabContent">
          <div class="tab-pane fade show active" id="information" role="tabpanel" aria-labelledby="information-tab">
            <div class="row">
              <div class="col-sm-6"><h3>Rates</h3>
                <table class="table table-striped">
                  <thead>
                    <tr>
                      <th>Hours of stay</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>12 hours</td>
                      <td class="text-success"><?php echo $r12?></td>
                    </tr>
                    <tr>
                      <td>24 hours</td>
                      <td class="text-success"><?php echo $r24?></td>
                    </tr>
                  </tbody>
                </table>
              </div>
            <div class="col-sm-6"><h3>Includes</h3>
              <table class="table table-striped">
                  <thead>
                    <tr>
                      <th>Included in the rent</th>
                      <th>ADD-ON</th>
                      <th>Price</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td><?php echo $inckaraoke?></td>
                      <td><?php echo $gasul?></td>
                      <td>200 php</td>
                    </tr>
                    <tr>
                      <td><?php echo $incbilliards?></td>
                    </tr>
                  </tbody>
                </table>
            </div>
            </div>
            <div class="row">
              <div class="col-sm-4"><h3>Rooms</h3>
                <table class="table table-striped">
                  <thead>
                    <tr>
                      <th>Type of room</th>
                      <th>Number of room/s</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>Ordinary room/s</td>
                      <td><?php echo $rmord?></td>
                    </tr>
                    <tr>
                      <td>Air-conditioned room/s</td>
                      <td><?php echo $rmair?></td>
                    </tr>
                  </tbody>
                </table>
              </div>
              <div class="col-sm-4"><h3>Pools</h3>
                <table class="table table-striped">
                  <thead>
                    <tr>
                      <th>Type of pool</th>
                      <th>Depth</th>
                      <th>Number of pool/s</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>Adult pool</td>
                      <td><?php echo $pldepthadult?></td>
                      <td><?php echo $pladult?></td>
                    </tr>
                    <tr>
                      <td>Kiddie pool</td>
                      <td><?php echo $pldepthkiddie?></td>
                      <td><?php echo $plkiddie?></td>
                    </tr>
                  </tbody>
                </table>
              </div>
              <div class="col-sm-4"><h3>Maximum Persons</h3>
                <ul style="list-style: none;">
                <li><?php echo $maxper?> PAX</li>
                </ul>
              </div>
            </div>
          </div>
          <!-- Pool Tab -->
          <div class="tab-pane fade" id="pool" role="tabpanel" aria-labelledby="pool-tab">
            <div class="row">
              <div class="col-6 mt-2">
                <h3 class="text-center">Adult Pool</h3>
                <img src="image/resort/alegreya/alegreya8.jpg" class="img-fluid" alt="Responsive image">
              </div>
              <div class="col-6 mt-2">
                <h3 class="text-center">Kiddie Pool</h3>
                <img src="image/resort/alegreya/alegreya9.jpg" class="img-fluid" alt="Responsive image">
              </div>
            </div>
          </div>
          <!-- Pool Tab -->
          <!-- Rooms Tab -->
          <div class="tab-pane fade" id="room" role="tabpanel" aria-labelledby="room-tab">
            <div class="row">
              <div class="col-6 mt-2">
                <h3 class="text-center">Air-conditioned Rooms</h3>
                <div class="row">
                  <div class="col-6 mt-2">
                    <img src="image/resort/alegreya/alegreya2.jpg" class="img-fluid" alt="Responsive image">
                  </div>
                  <div class="col-6 mt-2">
                    <img src="image/resort/alegreya/alegreya7.jpg" class="img-fluid" alt="Responsive image">
                  </div>
                </div>
              </div>
              <div class="col-6 mt-2">
                <h3 class="text-center">Ordinary Rooms</h3>
                <div class="row">
                  <div class="col-6 mt-2">
                    <img src="image/resort/alegreya/alegreya5.jpg" class="img-fluid" alt="Responsive image">
                  </div>
                  <div class="col-6 mt-2">
                    <img src="image/resort/alegreya/alegreya16.jpg" class="img-fluid" alt="Responsive image">
                  </div>
                </div>
              </div>
            </div>
          </div>
          <!-- Rooms Tab -->
        </div>
        
    </div>
  </div>
  <!-- Tabs -->
	<hr>
  <form action="reservation.php" method="post">
  <input type="hidden" name="resort" value=<?php echo $rname?>>
  <button class="btn btn-success btn-lg" href="reservation.php" role="button" name="value">Make Reservation</button>
  </form>
</div>

<!-- JUMBOTRON -->

<!-- FOOTER -->
<div class="footer">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <p>Copyright PRIVATE POOL RESERVATION 2019</p>
                </div>
            </div>
        </div>
    </div>
<!-- FOOTER -->

	  <!-- jQuery-2.2.4 js -->
    <script src="js/jquery/jquery-2.2.4.min.js"></script>
    <!-- Popper js -->
    <script src="js/bootstrap/popper.min.js"></script>
    <!-- Bootstrap-4 js -->
    <script src="js/bootstrap/bootstrap.min.js"></script>
    <!-- Bootstrap-4 js -->
    <script src="js/bootstrap/bootstrap.min.js"></script>

    <!-- SCRIPTS -->

    <!-- JQuery -->
    <script type="text/javascript" src="js/mdb/jquery-3.3.1.min.js"></script>
    <!-- Bootstrap tooltips -->
    <script type="text/javascript" src="js/mdb/popper.min.js"></script>
    <!-- MDB core JavaScript -->
    <script type="text/javascript" src="js/mdb/mdb.js"></script>
    <!-- Bootstrap core JavaScript -->
    <script src="js/adminjs/jquery.min.js"></script>
    <!-- Bootstrap Javascript -->
    <script src="js/bootstrap/js/bootstrap.bundle.min.js"></script> 





</body>
</html>