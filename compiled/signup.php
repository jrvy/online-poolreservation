<?php
include('db.php')
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
    <meta name="description" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<title>Sign Up form</title>
	<link rel="icon"  href="image/icon/user.png">


	<!-- Bootstrap CSS -->
    <link href="css/bootstrap/bootstrap.min.css" type="text/css" rel="stylesheet">
    <!-- MDB BOOTSTRAP -->
    <link rel="stylesheet" type="text/css" href="css/mdb/mdb.min.css">
    


</head>
<body>
<div class="row">
   	<div class="col-12">
		<form  method="POST" >
		  <div class="card mx-xl-5 mt-2 ">
		    <div class="card-body ">
		        <legend class="h4 text-center py-4 gradient-card-header young-passion-gradient white-text">Sign Up form<br><br>Personal Information</legend>
		        	<div class="md-form">
			            <label  class="grey-text font-weight-light">Username</label>
			            <input type="text" id="uname" name="uname" class="form-control" required autocomplete="off">
		        	</div>

		            <br>

		            <div class="md-form">
			            <label  class="grey-text font-weight-light">First name</label>
			            <input type="text" id="fname" name="fname" class="form-control" required autocomplete="off">
		        	</div>

		        	<br>

		            <div class="md-form">
			            <label  class="grey-text font-weight-light">Middle initial</label>
			            <input type="text" id="mname" name="mname" class="form-control" required autocomplete="off">
		        	</div>

		            <br>

		            <div class="md-form">
			            <label  class="grey-text font-weight-light">Last name</label>
			            <input type="text" id="lname" name="lname" class="form-control" required autocomplete="off">
		        	</div>

		            <br>

		            <div class="md-form">
			            <label for="defaultFormCardNameEx" class="grey-text font-weight-light">Contact Number</label>
			            <input type="text" id="cnum" name="cnum" class="form-control" required autocomplete="off"> 
		        	</div>

		            <br>

		            <div class="md-form">
			            <label  class="grey-text font-weight-light">Your email</label>
			            <input type="email" id="email" name="email" class="form-control" required autocomplete="off">
			        </div>

			        <br>

		            <div class="md-form">
			            <label  class="grey-text font-weight-light">Password</label>
			            <input type="password" id="pass" name="pass" class="form-control" required autocomplete="off">
			        </div>

			        <br>

		            <div class="md-form">
			            <label  class="grey-text font-weight-light">Confirm password</label>
			            <input type="password" id="pass2" name="pass2" class="form-control" required>
			        </div>
			        <input type="submit" name="submit1" class="btn night-fade-gradient">
		    </div><!-- card-body -->
		  </div><!-- card-body -->
		  <?php
			if(isset($_POST['submit1']))
							{
							
									$con=mysqli_connect("localhost","pansolpr","moimoi25","pansolpr_resort");
									$check="SELECT * FROM useracct WHERE email = '$_POST[email]'";
									$rs = mysqli_query($con,$check);
									$data = mysqli_fetch_array($rs, MYSQLI_NUM);
									if($data[0] > 1) {
										echo "<script type='text/javascript'> alert('Email already in Exists')</script>";
										}
										else{
										$check="SELECT * FROM useracct WHERE uname = '$_POST[uname]'";
										$rs = mysqli_query($con,$check);
										$data = mysqli_fetch_array($rs, MYSQLI_NUM);
										if ($data[0] > 1) {
											echo "<script type='text/javascript'> alert('Username Already in Exists')</script>";
										}
										else
										{	
											$uname = $_POST['uname'];
											$fname =$_POST['fname'];
											$mname = $_POST['mname'];
											$lname = $_POST['lname'];
											$mobilenumber = $_POST['cnum'];
											$email = $_POST['email'];
											$pass= $_POST['pass2'];

											require('textlocal.class.php');
											require('credential.php');

											$textlocal = new Textlocal(false, false, API_KEY);

											$numbers = array($mobilenumber);
											$sender = 'Pansol Private Pool Reservation';
											$otp=mt_rand(10000,99999);
											$message ="Hello ".$uname .".This is your CODE:".$otp;

											try {
											    $result = $textlocal->sendSms($numbers, $message, $sender);
											    setcookie('otp',$otp);
											    setcookie('uname',$uname);
											    setcookie('fname',$fname);
											    setcookie('mname',$mname);
											    setcookie('lname',$lname);
											    setcookie('cnum',$mobilenumber);
											    setcookie('email',$email);
											    setcookie('pass2',$pass);
											    header("location: acctverify.php");
											} catch (Exception $e) {
											    die('Error: ' . $e->getMessage());
											}
										}
									}
							}
		  ?>
		</form>
	</div><!-- col -->	
</div><!-- row -->        




	<!-- Js Validate -->
    <script src="bootstrap-validate-master/dist/bootstrap-validate.js"></script>
    <!-- jQuery-2.2.4 js -->
    <script src="js/jquery/jquery-2.2.4.min.js"></script>
    <!-- Popper js -->
    <script src="js/bootstrap/popper.min.js"></script>
    <!-- Bootstrap-4 js -->
    <script src="js/bootstrap/bootstrap.min.js"></script>
    <!-- Bootstrap-4 js -->
    <script src="js/bootstrap/bootstrap.min.js"></script>

    <!-- SCRIPTS -->

    <!-- JQuery -->
    <script type="text/javascript" src="js/mdb/jquery-3.3.1.min.js"></script>
    <!-- Bootstrap tooltips -->
    <script type="text/javascript" src="js/mdb/popper.min.js"></script>
    <!-- MDB core JavaScript -->
    <script type="text/javascript" src="js/mdb/mdb.js"></script>
    <!-- Bootstrap core JavaScript -->
    <script src="js/adminjs/jquery.min.js"></script>
    <!-- Bootstrap Javascript -->
    <script src="js/bootstrap/js/bootstrap.bundle.min.js"></script>

    
    <!-- Validators -->
    <script>
  // Basic Example
  	bootstrapValidate(['#fname','#mname','#lname'], 'alphanumeric:You can only input alphabet characters!');
  	bootstrapValidate('#cnum', 'numeric:You can only input numeric values! |startsWith:+63:Your phone number needs to start with +63 |max:13:Max.num is 13 digits');
  	bootstrapValidate('#email', 'email:Enter a valid email address');
  	bootstrapValidate('#pass2', 'matches:#pass:Your password does not match');
    </script>

</body>
</html>