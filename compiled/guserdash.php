<!DOCTYPE html>
<html lang="en">
<head>

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title>Private Pool Reservation</title>
	<link rel="icon" type="text/css" href="image/palmtree.png">

  <!-- Bootstrap core CSS -->
	<link rel="stylesheet" type="text/css" href="css/bootstrap/bootstrap.min.css">
  <!-- Custom CSS -->
	<link rel="stylesheet" type="text/css" href="userdes.css">
  <!-- FONT AWESOME CSS -->
	<link rel="stylesheet" type="text/css" href="fonts/font-awesome.css">
  <!-- MDB BOOTSTRAP -->
  <link rel="stylesheet" type="text/css" href="css/mdb/mdb.min.css">


</head>
<body>
    <nav class="user-navbar navbar fixed-top navbar-expand-lg navbar-dark orange ">
      <a class="navbar-brand" href="#">PRIVADO</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarNavDropdown">
        <ul class="navbar-nav w-100">
          <li class="nav-item active">
            <a class="nav-link active" href="guserdash.php">Menu<span class="sr-only">(current)</span></a>
          </li>
        </ul>  
      </div>
    </nav><!-- NAVBAR -->

<!-- CAROUSEL -->
    <div class="slider">
        <div class="container">
            <div id="carouselIndicators" class="carousel slide" data-ride="carousel">
              <ol class="carousel-indicators">
                <li data-target="#carouselIndicators" data-slide-to="0" class="active"></li>
                <li data-target="#carouselIndicators" data-slide-to="1"></li>
                <li data-target="#carouselIndicators" data-slide-to="2"></li>
                <li data-target="#carouselIndicators" data-slide-to="3"></li>
                <li data-target="#carouselIndicators" data-slide-to="4"></li>
                <li data-target="#carouselIndicators" data-slide-to="5"></li>
                <li data-target="#carouselIndicators" data-slide-to="6"></li>
                <li data-target="#carouselIndicators" data-slide-to="7"></li>
                <li data-target="#carouselIndicators" data-slide-to="8"></li>
                <li data-target="#carouselIndicators" data-slide-to="9"></li>
                <li data-target="#carouselIndicators" data-slide-to="10"></li>
                <li data-target="#carouselIndicators" data-slide-to="11"></li>
                <li data-target="#carouselIndicators" data-slide-to="12"></li>
                <li data-target="#carouselIndicators" data-slide-to="13"></li>
                <li data-target="#carouselIndicators" data-slide-to="14"></li>
              </ol>
              <div class="carousel-inner">
                <div class="carousel-item active">
                  <img src="image/resort/Valerie/valerie18.jpg" class="d-block w-100" alt="RESORT">
                    <div class="carousel-caption d-none d-md-block">
                      <h5>Valerie Private Pool</h5>
                    </div> 
                </div>
                <div class="carousel-item">
                  <img src="image/resort/carlos/carlos3.jpg" class="d-block w-100" alt="RESORT">
                    <div class="carousel-caption d-none d-md-block">
                      <h5>Carlos Private Pool</h5>
                    </div> 
                </div>
                <div class="carousel-item">
                  <img src="image/resort/elw/elw7.jpg" class="d-block w-100" alt="RESORT">
                    <div class="carousel-caption d-none d-md-block">
                      <h5>ELW Private Pool</h5>
                    </div> 
                </div>
                <div class="carousel-item">
                  <img src="image/resort/lagunablue/lagunablue2.jpg" class="d-block w-100" alt="RESORT">
                    <div class="carousel-caption d-none d-md-block">
                      <h5>Laguna Blue Private Pool</h5>
                    </div> 
                </div>
                <div class="carousel-item">
                  <img src="image/resort/lenzen/lenzen2.jpg" class="d-block w-100" alt="RESORT">
                    <div class="carousel-caption d-none d-md-block">
                      <h5>Lenzen Azy Private Pool</h5>
                    </div> 
                </div>
                <div class="carousel-item">
                  <img src="image/resort/rubi/rubi3.jpg" class="d-block w-100" alt="RESORT">
                    <div class="carousel-caption d-none d-md-block">
                      <h5>Rubi Rosa Private Pool</h5>
                    </div> 
                </div>
                <div class="carousel-item">
                  <img src="image/resort/vergara/vergara9.jpg" class="d-block w-100" alt="RESORT">
                    <div class="carousel-caption d-none d-md-block">
                      <h5>Vergara Private Pool</h5>
                    </div> 
                </div>
                <div class="carousel-item">
                  <img src="image/resort/Floredeliza/Floredeliza4.jpg" class="d-block w-100" alt="RESORT">
                    <div class="carousel-caption d-none d-md-block">
                      <h5>Floredeliza Private Pool</h5>
                    </div> 
                </div>
                <div class="carousel-item">
                  <img src="image/resort/Floredeliza/Floredeliza9.jpg" class="d-block w-100" alt="RESORT">
                    <div class="carousel-caption d-none d-md-block">
                      <h5>Floredeliza Private Pool</h5>
                    </div> 
                </div>
                <div class="carousel-item">
                  <img src="image/resort/INLG/INLG14.jpg" class="d-block w-100" alt="RESORT">
                    <div class="carousel-caption d-none d-md-block">
                      <h5>INLG Private Pool</h5>
                    </div> 
                </div>
                <div class="carousel-item">
                  <img src="image/resort/jocris3/jocris32.jpg" class="d-block w-100" alt="RESORT">
                    <div class="carousel-caption d-none d-md-block">
                      <h5>Villa Jocris 3 Private Pool</h5>
                    </div> 
                </div>
                <div class="carousel-item">
                  <img src="image/resort/Valerie/valerie19.jpg" class="d-block w-100" alt="RESORT">
                    <div class="carousel-caption d-none d-md-block">
                      <h5>Valerie Private Pool</h5>
                    </div> 
                </div>
                <div class="carousel-item">
                  <img src="image/resort/Villa Leonora/leonora18.jpg" class="d-block w-100" alt="RESORT">
                    <div class="carousel-caption d-none d-md-block">
                      <h5>Villa Leonora Private Pool</h5>
                    </div> 
                </div>
                <div class="carousel-item">
                  <img src="image/resort/Villa Leonora/leonora10.jpg" class="d-block w-100" alt="RESORT">
                    <div class="carousel-caption d-none d-md-block">
                      <h5>Villa Leonora Private Pool</h5>
                    </div> 
                </div>
                <div class="carousel-item">
                  <img src="image/resort/Villa Leonora/leonora9.jpg" class="d-block w-100" alt="RESORT">
                    <div class="carousel-caption d-none d-md-block">
                      <h5>Villa Leonora Private Pool</h5>
                    </div> 
                </div>
              </div>
              <a class="carousel-control-prev" href="#carouselIndicators" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
              </a>
              <a class="carousel-control-next" href="#carouselIndicators" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
              </a>
        </div>
    </div>
  </div>
<!-- CAROUSEL -->


<!-- MAIN CONTENT -->

<div class="main-content">
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <ul class="list-group user-list">
                  <li class="list-group-item  aqua-gradient" aria-disabled="true">Family Resorts</li>
                    <a href="alegreia.php" class="list-group-item cloudy-knoxville-gradient">Alegria Private Pool</a>
                    <a href="elw.php" class="list-group-item cloudy-knoxville-gradient">ELW Private Pool</a>
                    <a href="carlos.php" class="list-group-item cloudy-knoxville-gradient">Carlos Private Pool</a>
                    <a href="rubi.php" class="list-group-item cloudy-knoxville-gradient">Rubi Rosa Private Pool</a>
                    <a href="villajocris1.php" class="list-group-item cloudy-knoxville-gradient">Villa Jocris 1 Private Pool</a>
                    <a href="villajocris2.php" class="list-group-item cloudy-knoxville-gradient">Villa Jocris 2 Private Pool</a>
                    <a href="MandA.php" class="list-group-item cloudy-knoxville-gradient">M and A Private Pool</a>
                    <a href="marygrace.php" class="list-group-item cloudy-knoxville-gradient">Mary Grace Private Pool</a>
                    <a href="villacarmensita.php" class="list-group-item cloudy-knoxville-gradient">Villa Carmensita Private Pool</a>
                    <a href="inlg.php" class="list-group-item cloudy-knoxville-gradient">INLG Private Pool</a>
                    <a href="kayrense.php" class="list-group-item cloudy-knoxville-gradient">Kayrense Private Pool</a>
                </ul>
                <ul class="list-group">
                  <li class="list-group-item dusty-grass-gradient" aria-disabled="true">Debut, wedding receptions and large resorts</li>
                    <a href="villaleonora.php" class="list-group-item cloudy-knoxville-gradient">Villa Leonora Privat Pool</a>
                    <a href="valerie.php" class="list-group-item cloudy-knoxville-gradient">Valerie Private Pool</a>
                    <a href="floredeliza.php" class="list-group-item cloudy-knoxville-gradient">Floredeliza Private Pool</a>
                    <a href="villajocris3.php" class="list-group-item cloudy-knoxville-gradient">Villa Jocris 3</a>
                </ul>
            </div>
    <div class="col-md-8">
        <h5>Resorts</h5>
            <div class="row">

            <!-- Alegria -->     
              <div class="col-md-4">
                <div class="card card-cascade wider"> 
                  <div class="view view-cascade overlay">
                    <img class="card-img-top" src="image/resort/alegreya/alegreya11.jpg" alt="Resort">
                      <a href="#">
                        <div class="mask rgba-white-slight"></div>
                      </a>
                  </div>
                  <!-- Card content -->
                    <div class="card-body card-body-cascade">
                  <!-- Title -->
                    <h6 class="card-title text-center"><strong>Alegria Private Pool</strong></h6>
                  <!-- Subtitle -->
                    <h6 class="card-text red-text pt-3"><strong><h5>Location: </h5> Brgy Pansol Calamba City</strong></h6>
                  <!-- Text -->
                    <p class="card-text">A family type private resort on economic price.</p>
                    <p class="card-text"><strong>Rent:</strong> 8k-13k</p>
                    <a class="btn sunny-morning-gradient" href="guestresort/alegria.php"><span class="fa fa-eye"></span> View more</a>
                  </div>
                </div> 
              </div>
              <!-- Alegria -->

              <!-- Carlos -->
              <div class="col-md-4">
                <div class="card card-cascade wider"> 
                  <div class="view view-cascade overlay">
                    <img class="card-img-top" src="image/resort/carlos/carlos3.jpg" alt="Resort">
                      <a href="#">
                        <div class="mask rgba-white-slight"></div>
                      </a>
                  </div>
                  <!-- Card content -->
                    <div class="card-body card-body-cascade">
                  <!-- Title -->
                    <h6 class="card-title text-center"><strong>Carlos Private Pool</strong></h6>
                  <!-- Subtitle -->
                    <h6 class="card-text red-text pt-3"><strong><h5>Location: </h5> Brgy Pansol Calamba City</strong></h6>
                  <!-- Text -->
                    <p class="card-text">Large resort with giant slide, karaoke and billiards.</p>
                    <p class="card-text"><strong>Rent:</strong> 2.5k-5k</p>
                    <a class="btn sunny-morning-gradient" href="guestresort/carlos.php"><span class="fa fa-eye"></span> View more</a>
                  </div>
                </div> 
              </div>
              <!-- Carlos -->

              <!-- ELW -->
              <div class="col-md-4">
                <div class="card card-cascade wider"> 
                  <div class="view view-cascade overlay">
                    <img class="card-img-top" src="image/resort/elw/elw7.jpg" alt="Resort">
                      <a href="#">
                        <div class="mask rgba-white-slight"></div>
                      </a>
                  </div>
                  <!-- Card content -->
                    <div class="card-body card-body-cascade">
                  <!-- Title -->
                    <h6 class="card-title text-center"><strong>ELW Private Pool</strong></h6>
                  <!-- Subtitle -->
                    <h6 class="card-text red-text pt-3"><strong><h5>Location: </h5> Brgy Pansol Calamba City</strong></h6>
                  <!-- Text -->
                    <p class="card-text">Economic price with 2 pools and many others.</p>
                    <p class="card-text"><strong>Rent:</strong> 3k-7k</p>
                    <a class="btn sunny-morning-gradient" href="guestresort/elw.php"><span class="fa fa-eye "></span> View more</a>
                  </div>
                </div> 
              </div>
              <!-- ELW -->

              <!-- Laguna Blue -->
              <div class="col-md-4">
                <div class="card card-cascade wider"> 
                  <div class="view view-cascade overlay">
                    <img class="card-img-top" src="image/resort/lagunablue/lagunablue2.jpg" alt="Resort">
                      <a href="#">
                        <div class="mask rgba-white-slight"></div>
                      </a>
                  </div>
                  <!-- Card content -->
                    <div class="card-body card-body-cascade">
                  <!-- Title -->
                    <h6 class="card-title text-center"><strong>Laguna Blue Private Pool</strong></h6>
                  <!-- Subtitle -->
                    <h6 class="card-text red-text pt-3"><strong><h5>Location: </h5> Brgy Pansol Calamba City</strong></h6>
                  <!-- Text -->
                    <p class="card-text">Large resort with 3 pools,4 rooms and karaoke.</p>
                    <p class="card-text"><strong>Rent:</strong> 4k-8k</p>
                    <a class="btn sunny-morning-gradient" href="guestresort/lagunablue.php"><span class="fa fa-eye"></span> View more</a>
                  </div>
                </div> 
              </div>
              <!-- Laguna Blue -->

              <!-- Lenzen Azy -->
              <div class="col-md-4">
                <div class="card card-cascade wider"> 
                  <div class="view view-cascade overlay">
                    <img class="card-img-top" src="image/resort/lenzen/lenzen2.jpg" alt="Resort">
                      <a href="#">
                        <div class="mask rgba-white-slight"></div>
                      </a>
                  </div>
                  <!-- Card content -->
                    <div class="card-body card-body-cascade">
                  <!-- Title -->
                    <h6 class="card-title text-center"><strong>Lenzen Azy Private Pool</strong></h6>
                  <!-- Subtitle -->
                    <h6 class="card-text red-text pt-3"><strong><h5>Location: </h5> Brgy Pansol Calamba City</strong></h6>
                  <!-- Text -->
                    <p class="card-text">Large resort with 2 pools and karaoke.</p>
                    <p class="card-text"><strong>Rent:</strong> 5k-7k</p>
                    <a class="btn sunny-morning-gradient" href="guestresort/lenzen.php"><span class="fa fa-eye"></span> View more</a>
                  </div>
                </div> 
              </div>
              <!-- Lenzen Azy -->

              <!-- Rubi Rosa -->
              <div class="col-md-4">
                <div class="card card-cascade wider"> 
                  <div class="view view-cascade overlay">
                    <img class="card-img-top" src="image/resort/rubi/rubi3.jpg" alt="Resort">
                      <a href="#">
                        <div class="mask rgba-white-slight"></div>
                      </a>
                  </div>
                  <!-- Card content -->
                    <div class="card-body card-body-cascade">
                  <!-- Title -->
                    <h6 class="card-title text-center"><strong>Rubi Rosa Private Pool</strong></h6>
                  <!-- Subtitle -->
                    <h6 class="card-text red-text pt-3"><strong><h5>Location: </h5> Brgy Pansol Calamba City</strong></h6>
                  <!-- Text -->
                    <p class="card-text">Good for family outing or for student outings.Packed with karaoke and giant slides on pools.</p>
                    <p class="card-text"><strong>Rent:</strong> 2.5k-5k</p>
                    <a class="btn sunny-morning-gradient" href="guestresort/rubi.php"><span class="fa fa-eye"></span> View more</a>
                  </div>
                </div> 
              </div>
              <!-- Rubi Rosa -->

              <!-- Vergara -->
              <div class="col-md-4">
                <div class="card card-cascade wider"> 
                  <div class="view view-cascade overlay">
                    <img class="card-img-top" src="image/resort/vergara/vergara9.jpg" alt="Resort">
                      <a href="#">
                        <div class="mask rgba-white-slight"></div>
                      </a>
                  </div>
                  <!-- Card content -->
                    <div class="card-body card-body-cascade">
                  <!-- Title -->
                    <h6 class="card-title text-center"><strong>Vergara Private Pool</strong></h6>
                  <!-- Subtitle -->
                    <h6 class="card-text red-text pt-3"><strong><h5>Location: </h5> Brgy Pansol Calamba City</strong></h6>
                  <!-- Text -->
                    <p class="card-text">Large resort with 2 pools and 3 rooms.</p>
                    <p class="card-text"><strong>Rent:</strong> 3k-8k</p>
                    <a class="btn sunny-morning-gradient" href="guestresort/vergara.php"><span class="fa fa-eye"></span> View more</a>
                  </div>
                </div> 
              </div>
              <!-- Vergara -->

              <!-- Villa Jocris 1 -->
              <div class="col-md-4">
                <div class="card card-cascade wider"> 
                  <div class="view view-cascade overlay">
                    <img class="card-img-top" src="image/resort/jocris1/jocris14.jpg" alt="Resort">
                      <a href="#">
                        <div class="mask rgba-white-slight"></div>
                      </a>
                  </div>
                  <!-- Card content -->
                    <div class="card-body card-body-cascade">
                  <!-- Title -->
                    <h6 class="card-title text-center"><strong>Villa Jocris 1 Private Pool</strong></h6>
                  <!-- Subtitle -->
                    <h6 class="card-text red-text pt-3"><strong><h5>Location: </h5> Brgy Pansol Calamba City</strong></h6>
                  <!-- Text -->
                    <p class="card-text">Great for family outing and other occassions.</p>
                    <p class="card-text"><strong>Rent:</strong> 5k-8k</p>
                    <a class="btn sunny-morning-gradient" href="guestresort/villajocris1.php"><span class="fa fa-eye "></span> View more</a>
                  </div>
                </div> 
              </div>
              <!-- Villa Jocris 1 -->

              <!-- Villa Jocris 2 -->
              <div class="col-md-4">
                <div class="card card-cascade wider"> 
                  <div class="view view-cascade overlay">
                    <img class="card-img-top" src="image/resort/jocris2/jocris21.jpg" alt="Resort">
                      <a href="#">
                        <div class="mask rgba-white-slight"></div>
                      </a>
                  </div>
                  <!-- Card content -->
                    <div class="card-body card-body-cascade">
                  <!-- Title -->
                    <h6 class="card-title text-center"><strong>Villa Jocris 2 Private Pool</strong></h6>
                  <!-- Subtitle -->
                    <h6 class="card-text red-text pt-3"><strong><h5>Location: </h5> Brgy Pansol Calamba City</strong></h6>
                  <!-- Text -->
                    <p class="card-text">Great for family outing and other occassions.</p>
                    <p class="card-text"><strong>Rent:</strong> 5k-8k</p>
                    <a class="btn sunny-morning-gradient" href="guestresort/villajocris2.php"><span class="fa fa-eye "></span> View more</a>
                  </div>
                </div> 
              </div>
              <!-- Villa Jocris 2 -->

              <!-- Villa Jocris 3 -->
              <div class="col-md-4">
                <div class="card card-cascade wider"> 
                  <div class="view view-cascade overlay">
                    <img class="card-img-top" src="image/resort/jocris3/jocris30.jpg" alt="Resort">
                      <a href="#">
                        <div class="mask rgba-white-slight"></div>
                      </a>
                  </div>
                  <!-- Card content -->
                    <div class="card-body card-body-cascade">
                  <!-- Title -->
                    <h6 class="card-title text-center"><strong>Villa Jocris 3 Private Pool</strong></h6>
                  <!-- Subtitle -->
                    <h6 class="card-text red-text pt-3"><strong><h5>Location: </h5> Brgy Pansol Calamba City</strong></h6>
                  <!-- Text -->
                    <p class="card-text">Large resort with giant pool good for company outing or debut.</p>
                    <p class="card-text"><strong>Rent:</strong> 6k-12k</p>
                    <a class="btn sunny-morning-gradient" href="guestresort/villajocris3.php"><span class="fa fa-eye "></span> View more</a>
                  </div>
                </div> 
              </div>
              <!-- Villa Jocris 3 -->

              <!-- Santiago Medina -->
              <div class="col-md-4">
                <div class="card card-cascade wider"> 
                  <div class="view view-cascade overlay">
                    <img class="card-img-top" src="image/resort/SantiagoMedina/santiago14.jpg" alt="Resort">
                      <a href="#">
                        <div class="mask rgba-white-slight"></div>
                      </a>
                  </div>
                  <!-- Card content -->
                    <div class="card-body card-body-cascade">
                  <!-- Title -->
                    <h6 class="card-title text-center"><strong>Santiago Medina Private Pool</strong></h6>
                  <!-- Subtitle -->
                    <h6 class="card-text red-text pt-3"><strong><h5>Location: </h5> Miramonte Village,Brgy Pansol Calamba City</strong></h6>
                  <!-- Text -->
                    <p class="card-text">Large resort with giant pool good for company outing</p>
                    <p class="card-text"><strong>Rent:</strong> 7k-12k</p>
                    <a class="btn sunny-morning-gradient" href="guestresort/santiagomedina.php"><span class="fa fa-eye "></span> View more</a>
                  </div>
                </div> 
              </div>
              <!-- Santiago Medina -->

              <!-- M and A -->
              <div class="col-md-4">
                <div class="card card-cascade wider"> 
                  <div class="view view-cascade overlay">
                    <img class="card-img-top" src="image/resort/M and A/MandA11.jpg" alt="Resort">
                      <a href="#">
                        <div class="mask rgba-white-slight"></div>
                      </a>
                  </div>
                  <!-- Card content -->
                    <div class="card-body card-body-cascade">
                  <!-- Title -->
                    <h6 class="card-title text-center"><strong>M and A Private Pool</strong></h6>
                  <!-- Subtitle -->
                    <h6 class="card-text red-text pt-3"><strong><h5>Location: </h5> Miramonte Village,Brgy Pansol Calamba City</strong></h6>
                  <!-- Text -->
                    <p class="card-text">Great for family outing and other occassions.</p>
                    <p class="card-text"><strong>Rent:</strong> 10k</p>
                    <a class="btn sunny-morning-gradient" href="guestresort/MandA.php"><span class="fa fa-eye "></span> View more</a>
                  </div>
                </div> 
              </div>
              <!-- M and A  -->

              <!-- Mary Grace -->
              <div class="col-md-4">
                <div class="card card-cascade wider"> 
                  <div class="view view-cascade overlay">
                    <img class="card-img-top" src="image/resort/Mary Grace/marygrace7.jpg" alt="Resort">
                      <a href="#">
                        <div class="mask rgba-white-slight"></div>
                      </a>
                  </div>
                  <!-- Card content -->
                    <div class="card-body card-body-cascade">
                  <!-- Title -->
                    <h6 class="card-title text-center"><strong>Mary Grace Private Pool</strong></h6>
                  <!-- Subtitle -->
                    <h6 class="card-text red-text pt-3"><strong><h5>Location: </h5> Miramonte Village,Brgy Pansol Calamba City</strong></h6>
                  <!-- Text -->
                    <p class="card-text">Great for family outing.</p>
                    <p class="card-text"><strong>Rent:</strong> 7k-10k</p>
                    <a class="btn sunny-morning-gradient" href="guestresort/marygrace.php"><span class="fa fa-eye "></span> View more</a>
                  </div>
                </div> 
              </div>
              <!-- Mary Grace -->

              <!-- La Azu Thea -->
              <div class="col-md-4">
                <div class="card card-cascade wider"> 
                  <div class="view view-cascade overlay">
                    <img class="card-img-top" src="image/resort/La Azu Thea/lazuthea14.jpg" alt="Resort">
                      <a href="#">
                        <div class="mask rgba-white-slight"></div>
                      </a>
                  </div>
                  <!-- Card content -->
                    <div class="card-body card-body-cascade">
                  <!-- Title -->
                    <h6 class="card-title text-center"><strong>La Azu Thea Private Pool</strong></h6>
                  <!-- Subtitle -->
                    <h6 class="card-text red-text pt-3"><strong><h5>Location: </h5> Miramonte Village,Brgy Pansol Calamba City</strong></h6>
                  <!-- Text -->
                    <p class="card-text">Great for barkada outing and with free wifi.</p>
                    <p class="card-text"><strong>Rent:</strong> 8k-15k</p>
                    <a class="btn sunny-morning-gradient" href="guestresort/laazuthea.php"><span class="fa fa-eye "></span> View more</a>
                  </div>
                </div> 
              </div>
              <!-- La Azu Thea -->

              <!-- Villa Carmencita -->
              <div class="col-md-4">
                <div class="card card-cascade wider"> 
                  <div class="view view-cascade overlay">
                    <img class="card-img-top" src="image/resort/Villa Carmencita/carmencita.jpg" alt="Resort">
                      <a href="#">
                        <div class="mask rgba-white-slight"></div>
                      </a>
                  </div>
                  <!-- Card content -->
                    <div class="card-body card-body-cascade">
                  <!-- Title -->
                    <h6 class="card-title text-center"><strong>Villa Carmencita Private Pool</strong></h6>
                  <!-- Subtitle -->
                    <h6 class="card-text red-text pt-3"><strong><h5>Location: </h5> Miramonte Village,Brgy Pansol Calamba City</strong></h6>
                  <!-- Text -->
                    <p class="card-text">Great for family outing.</p>
                    <p class="card-text"><strong>Rent:</strong> 4k-7k</p>
                    <a class="btn sunny-morning-gradient" href="guestresort/villacarmensita.php"><span class="fa fa-eye "></span> View more</a>
                  </div>
                </div> 
              </div>
              <!-- Villa Carmencita -->

              <!-- INLG -->
              <div class="col-md-4">
                <div class="card card-cascade wider"> 
                  <div class="view view-cascade overlay">
                    <img class="card-img-top" src="image/resort/INLG/INLG14.jpg" alt="Resort">
                      <a href="#">
                        <div class="mask rgba-white-slight"></div>
                      </a>
                  </div>
                  <!-- Card content -->
                    <div class="card-body card-body-cascade">
                  <!-- Title -->
                    <h6 class="card-title text-center"><strong>INLG Private Pool</strong></h6>
                  <!-- Subtitle -->
                    <h6 class="card-text red-text pt-3"><strong><h5>Location: </h5> Miramonte Village,Brgy Pansol Calamba City</strong></h6>
                  <!-- Text -->
                    <p class="card-text">Great for family outing  and has many rooms.</p>
                    <p class="card-text"><strong>Rent:</strong> 3.5k-9k</p>
                    <a class="btn sunny-morning-gradient" href="guestresort/inlg.php"><span class="fa fa-eye "></span> View more</a>
                  </div>
                </div> 
              </div>
              <!-- INLG -->

              <!-- Valerie -->
              <div class="col-md-4">
                <div class="card card-cascade wider"> 
                  <div class="view view-cascade overlay">
                    <img class="card-img-top" src="image/resort/Valerie/Valerie18.jpg" alt="Resort">
                      <a href="#">
                        <div class="mask rgba-white-slight"></div>
                      </a>
                  </div>
                  <!-- Card content -->
                    <div class="card-body card-body-cascade">
                  <!-- Title -->
                    <h6 class="card-title text-center"><strong>Valerie Private Pool</strong></h6>
                  <!-- Subtitle -->
                    <h6 class="card-text red-text pt-3"><strong><h5>Location: </h5> Miramonte Village,Brgy Pansol Calamba City</strong></h6>
                  <!-- Text -->
                    <p class="card-text">Best place to celebrate debut,family reunion and company outings.</p>
                    <p class="card-text"><strong>Rent:</strong> 8k-15k</p>
                    <a class="btn sunny-morning-gradient" href="guestresort/valerie.php"><span class="fa fa-eye "></span> View more</a>
                  </div>
                </div> 
              </div>
              <!-- Valerie -->

              <!-- Kayrense -->
              <div class="col-md-4">
                <div class="card card-cascade wider"> 
                  <div class="view view-cascade overlay">
                    <img class="card-img-top" src="image/resort/Kayrense/kayrense7.jpg" alt="Resort">
                      <a href="#">
                        <div class="mask rgba-white-slight"></div>
                      </a>
                  </div>
                  <!-- Card content -->
                    <div class="card-body card-body-cascade">
                  <!-- Title -->
                    <h6 class="card-title text-center"><strong>Kayrense Private Pool</strong></h6>
                  <!-- Subtitle -->
                    <h6 class="card-text red-text pt-3"><strong><h5>Location: </h5> Miramonte Village,Brgy Pansol Calamba City</strong></h6>
                  <!-- Text -->
                    <p class="card-text">Great for family outing and barkada outing.</p>
                    <p class="card-text"><strong>Rent:</strong> 6k-9k</p>
                    <a class="btn sunny-morning-gradient" href="guestresort/kayrense.php"><span class="fa fa-eye "></span> View more</a>
                  </div>
                </div> 
              </div>
              <!-- Kayrense -->

              <!-- Floredeliza -->
              <div class="col-md-4">
                <div class="card card-cascade wider"> 
                  <div class="view view-cascade overlay">
                    <img class="card-img-top" src="image/resort/Floredeliza/Floredeliza2.jpg" alt="Resort">
                      <a href="#">
                        <div class="mask rgba-white-slight"></div>
                      </a>
                  </div>
                  <!-- Card content -->
                    <div class="card-body card-body-cascade">
                  <!-- Title -->
                    <h6 class="card-title text-center"><strong>Floredeliza Private Pool</strong></h6>
                  <!-- Subtitle -->
                    <h6 class="card-text red-text pt-3"><strong><h5>Location: </h5> Miramonte Village,Brgy Pansol Calamba City</strong></h6>
                  <!-- Text -->
                    <p class="card-text">Great venue for debut or pool party.</p>
                    <p class="card-text"><strong>Rent:</strong> 8k-15k</p>
                    <a class="btn sunny-morning-gradient" href="guestresort/floredeliza.php"><span class="fa fa-eye "></span> View more</a>
                  </div>
                </div> 
              </div>
              <!-- Floredeliza -->

              <!-- Villa Leonora -->
              <div class="col-md-4">
                <div class="card card-cascade wider"> 
                  <div class="view view-cascade overlay">
                    <img class="card-img-top" src="image/resort/Villa Leonora/leonora18.jpg" alt="Resort">
                      <a href="#">
                        <div class="mask rgba-white-slight"></div>
                      </a>
                  </div>
                  <!-- Card content -->
                    <div class="card-body card-body-cascade">
                  <!-- Title -->
                    <h6 class="card-title text-center"><strong>Villa Leonora Private Pool</strong></h6>
                  <!-- Subtitle -->
                    <h6 class="card-text red-text pt-3"><strong><h5>Location: </h5> Miramonte Village,Brgy Pansol Calamba City</strong></h6>
                  <!-- Text -->
                    <p class="card-text">Great venue for debut it can accomodate large number of people and also good for wedding receptions.</p>
                    <p class="card-text"><strong>Rent:</strong> 15k-25k</p>
                    <a class="btn sunny-morning-gradient" href="guestresort/villaleonora.php"><span class="fa fa-eye "></span> View more</a>
                  </div>
                </div> 
              </div>
              <!-- Villa Leonora -->
            </div> <!-- row -->

</div> <!-- col-md-8 -->

        </div> <!-- row -->
    </div> <!-- container -->
</div> <!-- Main Content -->


<!-- FOOTER -->
    <div class="footer">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <p>Copyright 2019</p>
                </div>
            </div>
        </div>
    </div>
<!-- FOOTER -->


	  <!-- jQuery-2.2.4 js -->
    <script src="js/jquery/jquery-2.2.4.min.js"></script>
    <!-- Popper js -->
    <script src="js/bootstrap/popper.min.js"></script>
    <!-- Bootstrap-4 js -->
    <script src="js/bootstrap/bootstrap.min.js"></script>
    <!-- All Plugins js -->
    <script src="js/others/plugins.js"></script>
    <!-- Active JS -->
    <script src="js/active.js"></script>

                      


</script>
</body>
</html>